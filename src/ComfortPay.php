<?php

ini_set('soap.wsdl_cache_enabled',0);
ini_set('soap.wsdl_cache_ttl',0);

/**
 * @method doCardTransaction
 * @method doCardCheck
 */
class ComfortPay {
	private $client;

	public $transactionStatus = null;

	public function __construct($params){
		if(!class_exists("SoapClient")){
			throw new ErrorException("Class 'SoapClient' not exists. Please contact your administrator.");
		}
		$params["params"]["exceptions"] = 0;
		$this->client = new SoapClient($params['wsdl'], $params['params']);
	}

	public function __call($name, $arg ){

		$return = @$this->client->__soapCall($name,$arg[0]);
		if (is_soap_fault($return)) {
			trigger_error("SOAP Fault: {$return->faultcode}: {$return->faultstring} | ".$return->transactionStatus, E_USER_WARNING);
			throw new SoapFaultException("SOAP Fault: {$return->faultcode}: {$return->faultstring} | ".$return->transactionStatus);
		}
		/*
		 *  Test case for súccess
		 *//*
			$return = new stdClass();
			$this->transactionStatus = $return->transactionStatus = "00";
			$return->transactionApproval = 123456;
		*/
		return $return;

	}
	public static function transactionStatusText($ts){
		if(is_object($ts)){
			$statusCode = $ts->transactionStatus;
		} elseif(is_string($ts)){
			$statusCode = $ts;
		}

		$text = array(
			"00" => "transakcia prijatá",
			"01" => "Volajte banku vydavateľa",
			"05" => "Transakcia zamietnutá bankou držiteľa karty",
			"12" => "Nepovolený typ transakcie. Volajte Tatra banku",
			"14" => "Transakcia zamietnutá bankou držiteľa karty",
			"15" => "Neexistujúci vydavateľ",
			"21" => "Transakcia nevykonaná",
			"51" => "Transakcia zamietnutá bankou držiteľa karty",
			"57" => "Transakcia zamietnutá bankou držiteľa karty. Volajte Tatra Banku",
			"62" => "Transakcia zamietnutá bankou držiteľa karty",
		);

		return ($text[$statusCode] !== null?$text[$statusCode]:$statusCode);
	}
}

class SoapFaultException extends Exception { }