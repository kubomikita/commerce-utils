<?php

class Timestamp {
	private $timestamp;
	public $format = 'U';

	private function parse($in){
		if(sizeof($in)==0){
			$this->timestamp=time();
		} else {
			$arg=$in;

			//dump($arg,sizeof($arg));
			if(count($arg)==1){
				if($arg[0] instanceof Timestamp){
					$this->timestamp=$arg[0]->timestamp;
				} else if(is_int($arg[0])){
					$this->timestamp=$arg[0];
				} else {
					$datestring=trim($arg[0]);
					//$this->timestamp=mktime(h,m,s,M,D,Y);
					if(preg_match('/^([0123456789]+)\-([0123456789]+)\-([0123456789]+)$/',$datestring,$dpart) ){
						$this->timestamp=mktime(0,0,0,$dpart[2],$dpart[3],$dpart[1]);
					}
					if(preg_match('/^([0123456789]+)\-([0123456789]+)\-([0123456789]+)\-([0123456789]+)\-([0123456789]+)$/',$datestring,$dpart) ){
						$this->timestamp=mktime($dpart[4],$dpart[5],0,$dpart[2],$dpart[3],$dpart[1]);
					}
					if(preg_match('/^([0123456789]+)\-([0123456789]+)\-([0123456789]+)\-([0123456789]+)\-([0123456789]+)\-([0123456789]+)$/',$datestring,$dpart) ){
						$this->timestamp=mktime($dpart[4],$dpart[5],$dpart[6],$dpart[2],$dpart[3],$dpart[1]);
					}
					if(preg_match('/^([0123456789]+)\.([0123456789]+)\.([0123456789]+)$/',$datestring,$dpart) ){
						$this->timestamp=mktime(0,0,0,$dpart[2],$dpart[1],$dpart[3]);
					}
					if(preg_match('/^([0123456789]+)\.([0123456789]+)\.([0123456789]+)\ ([0123456789]+)\:([0123456789]+)$/',$datestring,$dpart) ){
						$this->timestamp=mktime($dpart[4],$dpart[5],0,$dpart[2],$dpart[1],$dpart[3]);
					}
					if(preg_match('/^([0123456789]+)\.([0123456789]+)\.([0123456789]+)\ ([0123456789]+)\:([0123456789]+)\:([0123456789]+)$/',$datestring,$dpart) ){
						$this->timestamp=mktime($dpart[4],$dpart[5],$dpart[6],$dpart[2],$dpart[1],$dpart[3]);
					}
				};
			};


			if(count($arg)==3){ $this->timestamp=mktime(0,0,0,$arg[1],$arg[2],$arg[0]); };
			if(count($arg)==5){ $this->timestamp=mktime($arg[3],$arg[4],0,$arg[1],$arg[2],$arg[0]); };
			if(count($arg)==6){ $this->timestamp=mktime($arg[3],$arg[4],$arg[5],$arg[1],$arg[2],$arg[0]); };
		};
	}

	function __construct(){
		$arg=func_get_args();
		$this->parse($arg);
	}

	public function set(){
		if(func_num_args()==1 && is_array(func_get_arg(0))){
			$arg=func_get_arg(0);
			$this->parse($arg);
		} else {
			$arg=func_get_args();
			$this->parse($arg);
		};
	}

	public function shift($k){
		if(is_int($k)){
			$ts=$this->timestamp+$k;
			$T=new Timestamp($ts);
		} else {
			if(preg_match('/^([0123456789]+)[d]$/',$k,$kv) ){
				$ts=$this->timestamp+($kv[1]*86400);
				$T=new Timestamp($ts);
			};
			if(preg_match('/^([0123456789]+)[m]$/',$k,$kv) ){
				$D=$this->arr();
				$D['M']+=$kv[0];
				if($D['M']>12){ $D['Y']+=floor($D['M']/12); $D['M']=$D['M']%12; };
				$T=new Timestamp($D['Y'],$D['M'],$D['D'],$D['h'],$D['m'],$D['s']);
			};
			if(preg_match('/^([0123456789]+)[y]$/',$k,$kv) ){
				$D=$this->arr();
				$D['Y']+=$kv[0];
				$T=new Timestamp($D['Y'],$D['M'],$D['D'],$D['h'],$D['m'],$D['s']);
			};
			if(preg_match('/^[-]([0123456789]+)[d]$/',$k,$kv) ){
				$ts=$this->timestamp-($kv[1]*86400);
				$T=new Timestamp($ts);
			};
			if(preg_match('/^[-]([0123456789]+)[m]$/',$k,$kv) ){
				$D=$this->arr();
				$D['M']-=$kv[0];
				if($D['M']<1){ $D['Y']-=ceil((-1)*$D['M']/12); $D['M']=12-((-1*$D['M'])%12); };
				$T=new Timestamp($D['Y'],$D['M'],$D['D'],$D['h'],$D['m'],$D['s']);
			};
			if(preg_match('/^[-]([0123456789]+)[y]$/',$k,$kv) ){
				$D=$this->arr();
				$D['Y']-=$kv[0];
				$T=new Timestamp($D['Y'],$D['M'],$D['D'],$D['h'],$D['m'],$D['s']);
			};
		};
		if(func_num_args()>1 && func_get_arg(1)){
			$this->set($T->val());
		}
		return $T;
	}

	function __toString(){
		return (string)date($this->timestamp,$this->format);
	}

	public function show(){
		$format=$this->format;
		if(func_num_args()==1){ $format=func_get_arg(0); };
		return date($format,$this->timestamp);
	}

	public function val(){
		return $this->timestamp;
	}

	public function arr(){
		$dx=getdate($this->timestamp);
		$d['Y']=$dx['year'];
		$d['M']=$dx['mon'];
		$d['D']=$dx['mday'];
		$d['h']=$dx['hours'];
		$d['m']=$dx['minutes'];
		$d['s']=$dx['seconds'];
		return $d;
	}

	public function dayStart(){
		$D=$this->arr();
		$D['h']=0; $D['m']=0; $D['s']=0;
		$T=new Timestamp($D['Y'],$D['M'],$D['D'],$D['h'],$D['m'],$D['s']);
		return $T;
	}

	public function dayEnd(){
		$D=$this->arr();
		$D['h']=23; $D['m']=59; $D['s']=59;
		$T=new Timestamp($D['Y'],$D['M'],$D['D'],$D['h'],$D['m'],$D['s']);
		return $T;
	}

	public function monthStart(){
		$D=$this->arr();
		$D['D']=1; $D['h']=0; $D['m']=0; $D['s']=0;
		$T=new Timestamp($D['Y'],$D['M'],$D['D'],$D['h'],$D['m'],$D['s']);
		return $T;
	}

	public function monthEnd(){
		$D=$this->arr();
		$D['M']+=1; $D['D']=1; $D['h']=0; $D['m']=0; $D['s']=0;
		if($D['M']>12){ $D['M']-=12; $D['Y']+=1; };
		$T=new Timestamp($D['Y'],$D['M'],$D['D'],$D['h'],$D['m'],$D['s']);
		$T->shift(-1,1);
		return $T;
	}

	public function yearStart(){
		$D=$this->arr();
		$D['M']=1; $D['D']=1; $D['h']=0; $D['m']=0; $D['s']=0;
		$T=new Timestamp($D['Y'],$D['M'],$D['D'],$D['h'],$D['m'],$D['s']);
		return $T;
	}

	public function yearEnd(){
		$D=$this->arr();
		$D['Y']+=1; $D['M']=1; $D['D']=1; $D['h']=0; $D['m']=0; $D['s']=0;
		$T=new Timestamp($D['Y'],$D['M'],$D['D'],$D['h'],$D['m'],$D['s']);
		$T->shift(-1,1);
		return $T;
	}

}
if(!function_exists("Timestamp")) {
	function Timestamp() {
		$T   = new Timestamp();
		$arg = func_get_args();
		$T->set( $arg );

		return $T;
	}
}
