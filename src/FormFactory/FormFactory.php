<?php
/**
 * Tovarnicka na vykreslenie formularov. Samotny formular sa sklada z blokov,
 * kde su jednotlive polia. Tato trieda zaobstarava vykreslenie celeho formulara.
 *
 * @author Jakub Mikita <kubomikita@gmail.com>
 * @version 2.0
 */
class FormFactory {
    const PATTERN_EMAIL = "^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$";
    const PATTERN_ICO = "^[0-9]{8,8}$";
    const PATTERN_MOBIL_SK = '^[9][0-9]{8}$'; //[+][4][2][1]
    const PATTERN_MOBIL = '^[0-9]{8,}$'; //[+]
    const PATTERN_MOBIL_ADMIN = '^[+][0-9]{11,}$'; //
    const PATTERN_PSC_INTER = "^[a-zA-Z0-9]{4,10}$";
    const PATTERN_PSC = "^[0-9]{5}$";
    //const PATTERN_ALPHANUMERIC = "^[0-9\pL\s\.-]+$";
    //const PATTERN_ALPHANUMERIC = "^(.+)$";
    const PATTERN_ALPHANUMERIC = "^[\pL\pN.\s-]+$";
    const PATTERN_NUMERIC = "^[0-9]+$";

    const AjaxHandler = "ajaxHandler.php";

    const CsrfTokenName = "token";
    const CsrfTokenLifeTime = 600;

    private $blocks = array();
    // nazov formulara
    private $name;
    private $ajax = false;
    private $ajax_scroll = false;
    private $success_function;
    //true ak vsetky povinne polozky su vyplnene, inak false
    private $valid = true;
    private $showTitle = true, $formClass;
    private $method = "POST";
    private $token = "";
    private $protection = false;
    public $id, $class;

    static $instances = array();
    static $required=array();
    static $ErrorLog=array();  
    static $PublicErrorLog=array();
    private static $ajax_library = "/template/js/jquery.form.js";//"/protein_new/template/js/jquery.form.js";
    private static $ajax_script = "/template/js/formfactory.ajax.js";//;"/protein_new/template/js/formfactory.ajax.js";

    public function __construct($name = null) {
        //dump();
        $this->name = $name;//.count(self::$instances);//self::generateName($name);// $this->checkName($name);
        //dump($this->name,self::$instances);
        if ($name == null) {
            $this->name = self::generateName("form-factory");
            $this->showTitle = false;
        }
        $this->id = Format::seoName($this->name);//.(count(self::$instances)>0?count(self::$instances):"");
        //dump($this->checkId(Format::seoName($this->name)));

        self::$instances[] = $this;
        
        //dump(self::$instances);
        $method = "_".strtoupper($this->method);
        //dump($_SESSION);
        /*var_dump(!$_POST and $this->protection);
        if(!$_POST and $this->protection){
            $this->setToken();
            //var_dump($this->token);
        } /*else {
            if($this->checkToken($_POST[$this->id."_".self::CsrfTokenName],$_POST[$this->id."_".self::CsrfTokenName."_time"])){
                echo "vsio ok";
            } else {
                $this->setToken();
                echo "nieconieje vporiadku";
            }
        }*/
    }
    public function checkId($name){
        foreach (self::$instances as $key => $instance) {
            dump($key);
            if($instance->id == $name){
                return  $instance->id;
            }
            # code...
        }
        return $name;
    }
    public function addProtection(){
        if(!$this->ajax){
            $this->protection = true;
            if(!$_POST){
                $this->setToken();
            }
        }
        return $this;
    }
    private function setToken(){
        $this->token=$this->generateToken();
        $_SESSION[self::CsrfTokenName][$this->id] = $this->token;
    }
    private function checkToken($token,$token_time){
        $session_token = $_SESSION[self::CsrfTokenName][$this->id];
        if(time() > ($token_time + self::CsrfTokenLifeTime)){
            self::setError("token","Platnosť CSRF tokena vypršala, odošlite formulár znovu.",false);
            return false;
        }
        if($token != $session_token or $token == ""){
            $add="";
            if($token != ""){
                $add = "Refresh tejto stránky je zakázaný, odošlite formulár kliknutím na odosielacie tlačidlo!";
            }
            self::setError("token","Neplatný CSRF token. $add",false);
            return false;
        }

        return true;
    }
    private function generateToken() {
        return sha1(uniqid(md5($this->id)));//rand(1, 1e9);
    }

    private static function generateName($i) {
        if (count(self::$instances) > 0) {
            $l = strlen(count(self::$instances));
            return $i . count(self::$instances);
        }
        return $i;
    }
    public static function function_exists($function){
        if(strpos($function, "::") !== false){
            list($c,$m)=explode("::",$function);
            return method_exists($c, $m);
        }
        return function_exists($function);
    }
    public static function scripts(){
        $in = count(static::$instances);
        //if($in > 0){
            /*<script  src="'.static::$ajax_library.'"></script>*/
            echo '

                <script  src="'.static::$ajax_script.'"></script>
                

            ';
        //}
    }

    /**
    *   Výpis inicializačných errorov
    *   @return string
    */
    private function showErrors(){
        $ret="";
        if(!empty(static::$ErrorLog)){
            $ret.= '<div class="alert alert-info">';
            foreach(static::$ErrorLog as $message){
                $ret.= '<span class="glyphicon glyphicon-remove"></span> '.$message."<br>";
            }
            $ret.= '<br>Je možné, že formulár je nefunkčný.';        
            $ret.= '</div>';
        }
        if(!empty(static::$PublicErrorLog)){
            $ret.= '<div class="alert alert-danger">';
            foreach(static::$PublicErrorLog as $message){
                $ret.= '<span class="glyphicon glyphicon-remove"></span> '.$message."<br>";
            }
            //$ret.= '<br>Je možné, že formulár je nefunkčný.';        
            $ret.= '</div>';
        }
        return $ret;
    }
    public static function setError($key,$message,$develop=true){
        if($develop){
            static::$ErrorLog[$key] = $message;
        } else {
            static::$PublicErrorLog[$key] = $message;
        }
    }
    /**
    *   Setter pre nastavenie includovaneho skriptu pre ajax
    *   @param string $name
    *   @return \FormFactory
    */
    public function setAjaxFunctionsScript($script){
        if($this->checkScript($script,"Ajaxové funkcie") != ""){
            static::$ajax_script = $script;    
        }
        return $this;
    }

    public function setAjaxLibraryScript($script){
        if($this->checkScript($script,"Knižnica jQuery.AjaxForms") != ""){
            static::$ajax_library = $script;    
        }
        return $this;
    }
    /**
    *   Overí či zadaný file existuje a ak nie, zapise do error logu
    *   @param $filename - cesta k súboru
    *   @return $filename - cestu k suboru
    */
    public function checkScript($filename,$desc=""){
        if(strpos($filename, "/") === 0){
            $filename = $_SERVER["DOCUMENT_ROOT"]."/".substr($filename,1);
        }
        /*if(!file_exists($filename)){
            if($desc != ""){
                $desc = " <span class=\"text-danger bg-danger\">(".$desc.")</span>";
            }
            static::$ErrorLog["script_".md5($desc)]="<b>FormFactory:</b> skript <i><b>\"$filename\"</b></i> neexistuje$desc.";
        }*/
        return $filename;
    }
    public function setAjax($ajax=true){
        $this->ajax=$ajax;
        $this->checkScript(static::AjaxHandler,"Ajax PHP handler -> rieši presmerovanie ajaxové na kontroler");
        $this->checkScript(static::$ajax_library,"Knižnica jQuery.AjaxForms");
        $this->checkScript(static::$ajax_script,"Ajaxové funkcie");
        $this->protection = false;
        return $this;
    }
    public function getAjax(){
        return $this->ajax;
    }
    
    public function getId(){
        return $this->id;
    }
    public function getFormID() {
        return $this->id;
    }


    public function setMethod($method) {
        $this->method = $method;
        return $this;
    }

    public function setTitle($n){
        $this->name = $n;
        return $this;
    }

    public function setShowTitle($f) {
        $this->showTitle = $f;
        return $this;
    }

    public function setClass($f) {
        $this->class = $f;
        return $this;
    }
    public function setAjaxScrollOnSuccess($t){
        $this->ajax_scroll = $t;
        return $this;
    }
    public function setFormClass($f){
        $this->formClass = $f;
        return $this;
    }
    public function setOnSuccess($func) {
        $this->success_function = $func;
        return $this;
    }

    public function addBlock($block) {
        $this->blocks[] = $block;
    }

    public function draw() {
        if($this->success_function == null){
            static::setError("success_function","<b>FormFactory:</b> Žiadna <i><b>Success funkcia</b></i> neje pre tento formulár definovaná.");
        } else {
            if(!self::function_exists($this->success_function)){
                static::setError("success_function","<b>FormFactory:</b> Success funkcia <i><b>$this->success_function</b></i> neexistuje.");
            }
        }

        ob_start();
        $ret = "";
        $ret .= '<fieldset class="' . $this->class . '">';
        if ($this->showTitle){
            $ret .= '<legend>' . $this->name . '</legend>';
        }
        $ret .= $this->reactionToSuccess();
        $ret .= '<div id="'.$this->id.'_responseText" class="form-response-div"></div>';
        if($this->ajax != ""){
            //var_dump();
            //list($contr,$meth) = explode("::",$this->success_function);
            $ret .= '<form class="' . $this->formClass . '"  method="' . $this->method . '" data-func="'.$this->success_function.'" data-method="'.$this->method.'" id="' . $this->id . '"  onsubmit="javascript:ajax_form(\''.$this->id.'\','.(int)$this->ajax_scroll.');return false;" enctype="multipart/form-data" role="form">';
            $ret .= '<input type="hidden" name="ajax" value="1">';
            $ret .= '<input type="hidden" name="formId" value="'.$this->id.'">';
            //$ret .= '<noscript></form><form class="' . $this->formClass . '" method="' . $this->method . '" id="' . $this->id . '" enctype="multipart/form-data" role="form"></noscript>';
            $ret .= '<noscript><div class="alert">Pre správne fungovanie formulára si prosím zapnite <strong>JavaScript</strong>.</div></noscript>';
        } else {
            $ret .= '<form class="' . $this->formClass . '" method="' . $this->method . '" id="' . $this->id . '" enctype="multipart/form-data" role="form">';
        }

        foreach ($this->blocks as $block) {
            $ret .= $block->draw();
        }
        $ret .= <<<EOT
                <noscript><p>Vyplňte nospam: <input name="my_email" size="6" /></p></noscript>
                <script>
                $("#$this->id").append('<input type="hidden" name="my_email" value="no' + 'spam">');
                /*document.write('<input type="hidden" name="my_email" value="no' + 'spam" />');*/
                </script>  
EOT;
        if($this->protection){
            $ret .= '<input type="hidden" name="'.$this->id."_".self::CsrfTokenName.'" value="'.$this->token.'">';
            $ret .= '<input type="hidden" name="'.$this->id."_".self::CsrfTokenName.'_time" value="'.time().'">';
        }
        $ret .= '</form>';
        $ret .= "</fieldset>";
        echo $ret;
        $get=ob_get_contents();
        ob_end_clean();
        $return =  $this->showErrors() . $get ;

        return $return;
        //return $ret;
    }

    /**
     * Ak su vyplnene vsetky povinne polozky,spusti funkciu zadanu ako parameter
     * @param type $func nazov funkcie v tvare [NazovTriedy]::[NazovFunkcie],
     * ak ide o staticku funkciu
     */
    public function reactionToSuccess(){

        if($this->success_function != null and $this->ajax == false){
            ob_start();
            if (!isset($_POST["my_email"]) or $_POST['my_email'] != 'nospam') {
                return;
            }
            if ($_POST["form_id"] == $this->id) {
                $req = array();
                //var_dump($this->blocks);
                //dump($_POST);
                foreach ($this->blocks as $blocks) {
                    
                    foreach($blocks->getItems() as $item){
                        //dump($item->getName());
                        if ($_POST && $item->getRequired() && $_POST[$item->getName()] == '') {
                            $req[] = $item->getRequired(); // . '</div>';

                            $this->valid = false;
                        }
                        if(isset($_POST[$item->getName()])){
                            $ch=FormItem::checkPattern($item->getPattern(),$_POST[$item->getName()]);
                            if($ch!== null){
                                if($ch == false){
                                    if($_POST[$item->getName()] != ""){
                                        $req[] = $item->getPatternText();
                                    }
                                    $this->valid = false;
                                }
                            }
                            $item->setValue($_POST[$item->getName()]);
                        }
                        
                        //var_dump($ch);
                    }
                }
                if($this->protection){
                    $token_name = $this->id."_".self::CsrfTokenName;
                //if($_POST[$token_name]){
                    $compare =$this->checkToken($_POST[$token_name],$_POST[$token_name."_time"]);
                    unset($_POST[$token_name]);
                    unset($_POST[$token_name."_time"]);
                    //var_dump($compare);
                    $this->setToken();
                    if($compare === false){
                        return "";
                    }
                }

                if ($this->valid) {
                    unset($_POST["form_id"]); unset($_POST["my_email"]);
                    $post = array_merge($_POST, $_FILES);
                    //$e=explode("::",$this->success_function);
                    //var_dump($this->success_function,$post,method_exists($e[0],$e[1]));
                    //var_dump($e);
                    //$q=call_user_method($e[1], $e[0]);
                    //$q=call_user_func($this->success_function);
                    if(my_function_exists($this->success_function)){
                        call_user_func($this->success_function, $post, false, $this->method, $this->valid);
                    }
                    //var_dump($q);
                } else {
                    echo '<div class="alert alert-danger">';
                    foreach ($req as $r) {
                        echo $r . "<br>";
                    }
                    echo '</div>';
                }
            }
            $ob=ob_get_contents();
            ob_end_clean();

            return $ob;
        }/* else {
            return '<div class="alert alert-info"> </div>';
        }*/
    }

}
