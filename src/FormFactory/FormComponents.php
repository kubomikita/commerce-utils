<?php

/**
 * Textovy input
 */
class FormItemText extends FormItem {
    private $size;
    protected $type = "text";
    protected $progressCreated;
    
    function setSize($size) {
        $this->size = $size;
        return $this;
    }
    
    function setType($type) {
        $this->type = $type;
        return $this;
    }
    
    function getType() {
        return $this->type;
    }
    
    /**
     *  TENTO SETTER MUSI IST VZDY NA KONIEC resp. ZA setter setDivAttr();
     * @param type $val
     * @return \FormItemTextarea
     */
    function setEqualTo($field,$text){
        $this->equalTo = $field;
        $this->equalToText = $text;
        $ar = array("field"=>$this->equalTo,"text"=>$this->equalToText);
        $_SESSION["FormEqualTo"][$this->formID][$this->name] = $ar;
        //$_SESSION["FormEqualTo"][$this->formID][$this->name]["text"] = $this->equalToText;
        return $this;
    }
    function setMultilang($val){
        $this->multilang=$val;
        if($val){
            $this->addDivAttr("class","multilang");
        }
        return $this;
    }
    function getProgressBar(){
        $ret = "";
        //$this->main
        if($this->formObject->getAjax() and $this->type=="file" and !$this->progressCreated){
            $this->progressCreated = true;
            $ret .= '<div class="progress" id="progress_'.$this->formID.'">
                        <div class="bar"></div >
                        <div class="percent">0%</div >
                    </div>';
        }
        return $ret;
    }
    public function getHTML() {

        //var_dump();
        $ret='<label for="'.$this->formID.'-'.$this->name.'" class="'.$this->labelClass.'">'.$this->label.'</label>';
        if($this->multilang){
            $ret.='<div '.$this->getDivAttrs().'>'.FormBlock::generateInput($this). $this->getDesc(). '</div>';
        } else {
            $ret.='<div '.$this->getDivAttrs().'>
                        <input type="'.$this->type.'" id="'.$this->formID.'-'.$this->name.'" name="'.$this->name.'" value="'.$this->value.'" '.$this->getAttrs().' />'.$this->getDesc().$this->getProgressBar().'
                    </div>';
        }
        return $ret;
    }    
}

class FormItemHtml extends FormItem {
    protected $html;
    public function __construct($name,$label,$html,$formID){
        parent::__construct($name, $label, $formID);
        $this->html=$html;
    }
    public function getHTML(){
        //var_dump($this->labelClass);
        //$this->addDivAttr("class","simply-text");
        $ret='<label class="'.$this->labelClass.'">'.$this->label.'</label>';
        $ret.='<div '.$this->getDivAttrs().'>'.$this->html.$this->getDesc().'</div>';
        return $ret;
    }
}

class FormItemPlainHtml extends FormItem {
    protected $html,$class = "plain_html";
    public function __construct($html,$formID){
        $this->html = $html;
        $this->formID = $formID;
        $this->render=false;
    }
    /*public function setClass($c){
        $this->class = $c;
        return $this;
    }*/
    public function getHTML(){
        $ret = '<div '.$this->getAttrs().' >'.$this->html.'</div>';
        return $ret;
    }
}


/**
 * Tlacidlo pre odoslanie formulara
 */
class FormItemSubmit extends FormItem {
    
    public function __construct($label,$formId) {
        $this->render=false;
        $this->label = $label;
        $this->formId=$formId;
        $this->setAttr("class", "btn btn-success");
        $this->setDivAttr("class","form-actions text-right");
    }
    
    public function getHTML() {
        return '<div '.$this->getDivAttrs().'><button type="submit" name="form_id" value="'.$this->formId.'" '.$this->getAttrs().'>'.$this->label.'</button>'.$this->getDesc().'</div>';
    }    
}

/**
 * WYSIWYG Textarea
 */
class FormItemTextarea extends FormItem {
    private $rows;
    private $cols;
    
    public function __construct($name, $label, $formId, $rows=10, $cols=15) {
        parent::__construct($name, $label, $formId);
        $this->rows = $rows;
        $this->cols = $cols;
    }
    
    /**
     *  TENTO SETTER MUSI IST VZDY NA KONIEC resp. ZA setter setDivAttr();
     * @param type $val
     * @return \FormItemTextarea
     */
    function setMultilang($val){
        $this->multilang=$val;
        if($val){
            $this->addDivAttr("class","multilang");
        }
        return $this;
    }
    
    public function getHTML() {
        $ret= '<label for="'.$this->formID.'-'.$this->name.'" class="'.$this->labelClass.'">'.$this->label.'</label>';
        if($this->multilang){
            $ret.='<div '.$this->getDivAttrs().'>'.FormBlock::generateTextArea($this).$this->getDesc().'</div>';
        } else {
            $ret.='<div '.$this->getDivAttrs().'><textarea '.$this->getAttrs().' id="'.$this->formID.'-'.$this->name.'" name='.$this->name.'>'.$this->value.'</textarea>'.$this->getDesc().'</div>';
        }
        return $ret;
    }    
}

/**
 * Select
 */
class FormItemSelect extends FormItem {
    
    // Zoznam poloziek
    private $items = array();
    private $selected=array();
    
    public function __construct($name, $label,$formId, $items) {
        parent::__construct($name, $label,$formId);
        $this->items = $items;
    }
    public function setSelected($val){
        if(!is_array($val)) $val=array($val);
        $this->selected=$val;
        return $this;
    }
    public function getHTML() {
        $ret = "";
        $ret .=  '<label for="'.$this->formID.'-'.$this->name.'" class="'.$this->labelClass.'">'.$this->label.'</label>';
        $ret .=  '<div '.$this->getDivAttrs().'><select id="'.$this->formID.'-'.$this->name.'" name="'.$this->name.'" '.$this->getAttrs().'>';
        foreach ($this->items as $item => $key) {
            if(is_array($key)){
                //var_dump($item,$key);
                $ret .= "<optgroup label=\"$item\">";
                foreach($key as $k=>$v){
                    $s=(in_array($k, $this->selected))?"selected":"";
                    $ret .=  '<option value="'.$k.'" '.$s.'>'.$v.'</option>';
                }
                $ret .= "</optgroup>";
            } else {
                $s=(in_array($item, $this->selected))?"selected":"";
                $ret .=  '<option value="'.$item.'" '.$s.'>'.$key.'</option>';
            }
        }
        
        $ret .=  '</select>'.$this->getDesc().'</div>';
        
        return $ret;        
    }   
    
}

/**
 * Radio
 */
class FormItemRadio extends FormItem {
    private $items = array();
    private $checked="";
    private $itemBlock = false;
    private $itemBlockAttr = array();
    
    public function __construct($name, $label,$formId, $items) {
        parent::__construct($name, $label,$formId);

        /*$this->label = $label;
        if(is_array($label)){
            $this->label=$label[0];
            $this->labelClass=$label[1];
        }*/
        if(is_array($this->label)){
            $this->labelClass = $this->label[1];
            $this->label = $this->label[0];

        } else {
            $this->label = $label;
            if(is_array($label)){
                $this->label=$label[0];
                $this->labelClass=$label[1];
            }
        }
        //dump($label,$this->labelClass,$this->label);
        $this->items = $items;
    }
    
    public function setChecked($val){
        $this->checked=$val;
        return $this;
    }
    public function setItemBlock($val){
        $this->itemBlock = $val;
        return $this;
    }
    public function setItemBlockAttr($name,$value){
        $this->itemBlockAttr[$name] = $value;
        return $this;
    }
    public function getAttrs($id=null){
        //dump(parent::getAttrs());
        //dump($this->attrs);
        //if( is_array($this->attrs)){
        //$ret= parent::getAttrs();
        //$ret = parent::getAttrs();
            foreach($this->attrs as $key =>$values){

                if(is_array($values)){
                    foreach($values as $vid=>$attr){
                        //dump($vid==$id,$attr);
                        if($id == $vid){
                            $ret.= " $key=\"$attr\" ";
                            
                        } else {
                            continue;
                        }
                    }
                } else {
                    $ret.=" $key=\"$values\" ";
                }
            }
            if($this->ajax_object instanceof FormAjaxEvent){
                $ret .= $this->ajax_object->getItemAttr();
            }

            //dump($ret);
            return $ret;
        //}
    }
    public function getHTML() {
        $ret = '<label class="'.$this->labelClass.'">'.$this->label.'</label> <div '.$this->getDivAttrs().">";
        $i = 0;
        foreach ($this->items as $item => $key) {
            $ch=($item===$this->checked)?"checked":"";
            //dump($this->checked,$item,$key,$ch);
            if($this->itemBlock) {
                $ret.="<div ";
                if(!empty($this->itemBlockAttr)){
                    foreach ($this->itemBlockAttr as $name => $value) {
                        $ret.= $name.'="'.$value.'" ';
                    }
                }
                $ret.=">";
            }
            $ret .= '<input type="radio" value="'.$item.'" id="'.$this->formID.'-'.$this->name.'_'.$i.'" name="'.$this->name.'" '.$this->getAttrs($item).' '.$ch.'><label for="'.$this->formID.'-'.$this->name.'_'.$i.'">'.$key.'</label>                ';
            if($this->itemBlock) {$ret.="</div>";}
            $i++;
        }
        $ret.=$this->getDesc()."</div>";
        return $ret;
    }    
}

class FormItemHidden extends FormItem {
    
    public function __construct($name, $value,$formId) {
        parent::__construct($name, $label,$formId);
        $this->value = $value;
        $this->render=false;
    }
    public function getHTML() {
        return '
          <input type="hidden" id="'.$this->formID.'-'.$this->name.'" name="'.$this->name.'" value="'.$this->value.'" '.$this->getAttrs().'/>';  
    }    
}

?>
