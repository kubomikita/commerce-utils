<?php
/**
 * Trieda reprezentujca blok vo formulary. Do bloku su vkladane polozky formulara
 * a nasledne je potrebne tento blok vlozit do formulara.
 *
 * @author Jakub Mikita <kubomikita@gmail.com>
 * @version 1.0
 */
class FormBlock {

    // pole poloziek vo formulary
    private $items = array();
    // pocitadlo poloziek
    private $counter = 0;
    private $blockAttrs = array();
    private $beforeElement, $afterElement, $labelClass, $elementClass, $itemClass; 
    private $id;
    private $title=null;
    private $wizard=false;
    //private $render=true;
    
    public function __construct($formID) {
        $this->id = $formID;
    }

    public function getItems(){
        return $this->items;
    }

    public function addText($name, $label) {
        $this->items[$this->counter] = new FormItemText($name, $this->getLabelArray($label), $this->id);
        $this->defaultElementClass($this->items[$this->counter]);
        $this->defaultItemClass($this->items[$this->counter]);
        return $this->items[$this->counter++];
    }

    public function addSubmit($label = " Odoslať ") {
        $this->items[$this->counter] = new FormItemSubmit($label, $this->id);
        return $this->items[$this->counter++];
    }

    public function addTextarea($name, $label, $rows = 5, $cols = 30) {
        $this->items[$this->counter] = new FormItemTextarea($name, $this->getLabelArray($label), $this->id, $rows, $cols);
        $this->defaultElementClass($this->items[$this->counter]);
        $this->defaultItemClass($this->items[$this->counter]);
        return $this->items[$this->counter++];
    }

    public function addSelect($name, $label, $items) {
        $this->items[$this->counter] = new FormItemSelect($name, $this->getLabelArray($label), $this->id, $items);
        $this->defaultElementClass($this->items[$this->counter]);
        $this->defaultItemClass($this->items[$this->counter]);
        return $this->items[$this->counter++];
    }

    public function addRadio($name, $label, $items) {
        $this->items[$this->counter] = new FormItemRadio($name, $this->getLabelArray($label), $this->id, $items);
        $this->defaultElementClass($this->items[$this->counter]);
        $this->defaultItemClass($this->items[$this->counter]);
        return $this->items[$this->counter++];
    }

    public function addHidden($name, $value) {
        $this->items[$this->counter] = new FormItemHidden($name, $value, $this->id);
        return $this->items[$this->counter++];
    }

    public function addHTML($name, $label, $html) {
        $this->items[$this->counter] = new FormItemHtml($name, $this->getLabelArray($label), $html, $this->id);
        $this->defaultElementClass($this->items[$this->counter]);
        $this->defaultItemClass($this->items[$this->counter]);
        return $this->items[$this->counter++];
    }

    public function addPlainHTML($html) {
        $this->items[$this->counter] = new FormItemPlainHtml($html, $this->id);
        $this->defaultElementClass($this->items[$this->counter]);
        $this->defaultItemClass($this->items[$this->counter]);
        return $this->items[$this->counter++];
    }
    private function getLabelArray($label) {
        if ($this->labelClass != "") {
            return array($label, $this->labelClass);
        }
        return $label;
    }

    private function defaultElementClass($object) {
        if ($this->elementClass != "") {
            $object->setDivAttr("class", $this->elementClass);
            return true;
        }
        return false;
    }
    private function defaultItemClass($object){
        if($this->itemClass != "") {
            $object->setAttr("class",$this->itemClass);
            return true;
        }
        return false;
    }

    public function setAttr($name, $value) {
        $this->blockAttrs[$name] = $value;
        return $this;
    }

    public function setElementParent($b, $a) {
        $this->beforeElement = $b;
        $this->afterElement = $a;
    }

    public function setElementClass($c) {
        $this->elementClass = $c;
    }
    public function setItemClass($c){
        $this->itemClass=$c;
    }

    public function setLabelClass($c) {
        $this->labelClass = $c;
    }
    public function setTitle($title){
        $this->title=$title;
    }
    public function setWizard($w){
        $this->wizard=$w;
    }
    /**
     * Vrati HTML kod bloku
     */
    public function draw() {
        $ret = "";
        if($this->wizard){
            $ret.="<h1 class=\"form-wizard-header\" style=\"display:none;\">".$this->title."</h1>";
        }
        $ret .= "<div ";
        foreach ($this->blockAttrs as $attr => $value) {
            $ret .= $attr . '="' . $value . '" ';
        }
        $ret .= ">";
        if($this->title!=null and $this->wizard == false){
            $ret.= "<legend>$this->title</legend>";
        }
        foreach ($this->items as $item) {
            if ($item->render) {
                $ret .= $this->beforeElement . $item->getHTML() . $this->afterElement;
            } else {
                $ret .= $item->getHTML();
            }
        }
        $ret .= "</div>";
        return $ret;
    }

    public static function generateInput($item) {

        foreach (Language::fetchAll() as $lang) {
            if ($lang->id == Core::$conf["defaultLanguage"]) {
                $isActive = "button_lang_active";
                $style = "";
                $def = true;
            } else {
                $isActive = "";
                $style = 'style="display:none;"';
                $def = false;
            }
            //var_dump($item->getAttrs());
            $attr = $item->getAttrs();
            if (!$def) {
                if (strpos($item->getAttrs(), "required") !== false) {
                    $attr = str_replace("required=\"required\"", "", $item->getAttrs());
                }
            }
            $buttons.="<a href=\"#" . $item->getName() . "[$lang->id]\" class=\"button_lang $isActive\" $rel><img alt=\"$lang->id\" src=\"$lang->image\"></a>";
            $inputs .= '<input type="' . $item->getType() . '" id="' . $item->getFormID() . '-' . $item->getName() . '" name="' . $item->getName() . '[' . $lang->id . ']' .
                    '" value="' . $item->getValue($lang->id) . '" ' .
                    $attr . ' ' . $style . ' ' . $isActive . ' />';
        }

        $ret .= $inputs .
                '<div class="multilang_buttons">' .
                $buttons .
                '</div>';

        return $ret;
    }

    public static function generateTextArea($item) {
        $ret = "";

        foreach (Language::fetchAll() as $lang) {
            $rel = " rel=\"" . Core::$conf["defaultEditor"] . "\"";
            if ($lang->id == Core::$conf["defaultLanguage"]) {
                $isActive = "button_lang_active";
                $style = "";
            } else {
                $isActive = "";
                $style = 'style="display:none;"';
            }
            $buttons.="<a href=\"#" . $item->getFormID() . "-" . $item->getName() . "[$lang->id]\" aria-input=\"" . $item->getFormID() . '-' . $item->getName() . "-" . $lang->id . "\" class=\"button_lang $isActive\" $rel><img alt=\"$lang->id\" src=\"$lang->image\"></a>";
            $inputs .= '<textarea ' . $item->getAttrs() . ' id="' . $item->getFormID() . '-' . $item->getName() . '[' . $lang->id . ']" name="' . $item->getName() . '[' . $lang->id . ']' . '" ' . $style . ' ' . $isActive . '>' .
                    $item->getValue($lang->id) . '</textarea>';
        }

        $ret .= $inputs .
                '<div class="multilang_buttons">' .
                $buttons .
                '</div>';

        return $ret;
    }

}
