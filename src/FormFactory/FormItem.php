<?php
/**
 * Abstraktna trieda ako model pre komponenty formulara
 * 
 * @author Jakub Mikita <kubomikita@gmail.com>
 * @version 1.0
 */
abstract class FormItem {

    protected $name;
    protected $label;
    protected $labelClass;
    protected $value = null;//array();
    protected $attrs = array();
    protected $divAttrs =array();
    protected $required = null;
    protected $formID;
    protected $formObject;
    protected $multilang=false;
    protected $requiredLabel='<span class="form-required">*</span>';
    public $render=true;
    protected $desc;
    protected $ajax_object;
    protected $id;
    protected $pattern;
    protected $patternText;

    public function __construct($name, $label,$formId=null) {
        //var_dump($formId);
        $this->name = $name;
        $this->formID = $formId;
        //$this->render = true;
        //$this->formName=
        //dump($this->label);
        /*if(is_array($this->label)){

        } else {*/
            $this->label = $label;
            if(is_array($label)){
                $this->label=$label[0];
                $this->labelClass=$label[1];
            }
        //}
        $this->setFormObject();
        //$rs = $this->getRequiredSession();
        /*foreach($_SESSION as $k=>$v){
            if(strpos($k, "Form") === 0){
                //dump($v);
                foreach($v as $k1=>$v1){
                    ///dump($v1);
                    if(empty($v1)){
                        unset($_SESSION[$k][$this->formID]);
                    } else {
                        $_SESSION[$k][$this->formID] = array();
                        unset($_SESSION[$k][$this->formID]);
                    }
                }
                //
            }
        }*/
        //dump($_SESSION);
        //var_dump(count($rs),$rs);
        //$_SESSION["FormRequired"][$this->formID]=array();
        unset($_SESSION["FormRequired"][$this->formID][$this->name]);
        $_SESSION["FormEqualTo"][$this->formID][$this->name]=array();
        unset($_SESSION["FormEqualTo"][$this->formID][$this->name]);
        $_SESSION["FormDependsOn"][$this->formID][$this->name]=array();
        unset($_SESSION["FormDependsOn"][$this->formID][$this->name]);
        //var_dump($name,$this->formID,$_SESSION["FormRequired"][$this->formID]);
        //echo "<hr>";//$_SESSION["FormRequired"][$this->formID]);
        $_SESSION["FormPattern"][$this->formID][$this->name]=array();
        //$_SESSION["FormPattern"][$this->formID]=array();
        unset($_SESSION["FormPattern"][$this->formID][$this->name]);
    }
    private function setFormObject(){
        foreach(FormFactory::$instances as $i){
            if($i->id == $this->formID){
                $this->formObject=$i;
                break;
            }
        }

    }
    private function getRequiredSession(){
        return $_SESSION["FormRequired"][$this->formID];
    }

    public function setDependsOn($name){
        $this->dependsOn = $name;
        $this->setAttr("data-depends",$name);
        $_SESSION["FormDependsOn"][$this->formID][$this->name] = $this->dependsOn;
        return $this;
    }
    public function setName($name) {
        $this->name = $name;
        return $this;
    }
    public function setId($id){
        $this->id=$id;
        return $this;
    }
    public function setLabel($label) {
        $this->label = $label;
        return $this;
    }

    public function setValue($value) {
        $this->value = $value;
        return $this;
    }

    public function setAttr($name, $value) {
        $this->attrs[$name] = $value;
        return $this;
    }
    
    public function setDivAttr($name, $value) {
        //var_dump($this->divAttrs[$name]==null);
        //if($this->divAttrs[$name]==null){
            $this->divAttrs[$name] = $value;
        /*}/* else{
            $this->divAttrs[$name] .= " ".$value;
        }*/
        return $this;
    }
    
    /**
     * Popis polozky. Je vhodne uviest, co sa ma do tejto polozky vlozit.
     * @param type $text
     */
    public function setDesc($text) {
        $this->desc = $text;
        return $this;
    }
    
    /**
     * Vrati hotovy HTML element popisu polozky
     * @return string
     */
    public function getDesc() {
        $ret ="";
        if($this->ajax_object instanceof FormAjaxEvent){
            $ret .= $this->ajax_object->getAjaxHtml();
        }
        if(strlen($this->desc)>0){
            $ret .= '<span class="help-block hidden-xs">';
            $ret .= $this->desc;
            $ret .= '</span>';
            
        }
        //var_dump($this->name,$this->formObject->getAjax());
        /*if($this->formObject->getAjax() and $this->type=="file"){
            $ret .= "progress bar";
        }*/
        return $ret;
    }
    /**
     * Prida k div atributu nieco
     * 
     * @param string $name
     * @param string $value
     * @return \FormItem
     */
    public function addDivAttr($name,$value){
        $this->divAttrs[$name] .= " ".$value;
        return $this;
    }
    public function setAjaxEvent($ajax_object){
        $this->ajax_object = $ajax_object;
        $this->ajax_object->setInputName($this->name);
        return $this;
    }
    public function getAjaxEvent(){
        return $this->ajax_object;
    }
    public function setRequired($text,$pattern="") {
        $this->required = $text;
        $this->label=$this->label.' '.$this->requiredLabel;
        $_SESSION["FormRequired"][$this->formID][$this->name] = $this->required;
        //$this->setAttr("required","required");
        //FormFactory::$required[$this->formID][]=$this->name;
        return $this;
    }

    public function setPattern($text,$pattern){
        $this->pattern = $pattern;
        $this->patternText = $text;
        $_SESSION["FormPattern"][$this->formID][$this->name]["patternText"] = $this->patternText;
        $_SESSION["FormPattern"][$this->formID][$this->name]["pattern"] = $this->pattern;
        return $this;
    }
    public function getPattern(){
        return $this->pattern;
    }
    public function getPatternText(){
        return $this->patternText;
    }
    public static function checkPattern($pattern,$value){
        if($pattern != null){
            $pattern='/'.$pattern.'/u';
            $res=preg_match($pattern, $value,$arr);
            //var_dump($pattern,$res,$arr);

            if(count($arr) > 0){
                return true;
            }
            return false;
        }
    }

    public function setRequired2($text,$pattern="") {
        $this->required = $text;
        $this->setAttr("required","required");
        $this->label=$this->label/*.' '.$this->requiredLabel*/;
        return $this;
    }
    
    public function getRequired() {
        return $this->required;
    }
    
    public function getName() {
        return $this->name;
    }
    
    /**
     * 
     * @param type $lang ID jazyka, napr. 'sk' alebo 'en'
     * @return type
     */
    public function getValue($lang) {
        return $this->value[$lang];
    }
    
    public function getFormID() {
        return $this->formID;
    }

    public function getAttrs() {
        $ret = " ";
        foreach ($this->attrs as $attr => $value) {
            $ret .= $attr.'="'.$value.'" ';
        }
        if($this->ajax_object instanceof FormAjaxEvent){
            $ret .= $this->ajax_object->getItemAttr();
        }
        return $ret;
    }
    
    protected function getDivAttrs() {
        $ret = " ";
        foreach ($this->divAttrs as $attr => $value) {
            $ret .= $attr.'="'.$value.'" ';
        }
        return $ret;
    }

    public abstract function getHTML();
    
    public static function multilang() {}
}

?>
