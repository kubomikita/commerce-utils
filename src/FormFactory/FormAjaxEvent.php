<?php /**
 * Plugin pre FormFactory ktory umoznuje pridavat jednotlivím poliam vo Formulari Ajaxové eventy
 *
 * @author Jakub Mikita <kubomikita@gmail.com>
 * @version 1.0
 */

class FormAjaxEvent {

	protected $formId;
	protected $event;
	protected $function;
	protected $responseDiv;
	protected $inputName;
	protected $inputId;
	protected $button;
	protected $buttonAttr=array();
	protected $checkAfter = 0;
	protected $noScroll = false;

	public function __construct($formId){
		$this->formId = $formId;
		$this->responseDiv = $this->formId."_responseText";
		//$this->checkGeneratingId();
	}
	public function setInputName($name){
		$this->inputName=$name;
		//$this->checkGeneratingId();
		return $this;
	}
	public function setButton($button){
		$this->button = $button;
		return $this;
	}
	public function setFunction($func){
		$this->function=$func;
		if(!FormFactory::function_exists($func)){
			FormFactory::setError("AjaxEvent_function","<b>FormAjaxEvent:</b> Success funkcia <i><b>$func</b></i> neexistuje");
		}
		return $this;
	}
	public function setCheckAfter($after){
		$this->checkAfter = $after;
		return $this;
	}
	public function setNoScroll($after){
		$this->noScroll = $after;
		return $this;
	}
	public function setLoader($element){
		$this->loader = $element;
		return $this;
	}
	public function setEvent($event){
		$this->event=$event;
		$this->stringifyEvent();
		return $this;
	}
	public function setResponseDiv($d){
		$this->responseDiv=$d;
		return $this;
	}
	public function checkGeneratingId(){
		//var_dump($this->formId);
		if($this->formId != null and $this->inputName != null){
			$this->inputId = $this->formId."_".$this->inputName;
		} else {
			FormFactory::setError("AjaxEvent_id","<b>FormAjaxEvent:</b> nepodarilo sa vytvoriť <i><b>inputId</b></i>, pravdepodobne ste nezadali <span class=\"text-danger bg-danger\">inputName</span>");
			//var_dump(FormFactory::$ErrorLog);
		}
	}
	private function stringifyEvent(){
		if(substr($this->event,0,2) != "on"){
			$this->event = "on".$this->event;
		}
		return $this->event;
	}
	public function setButtonAttr($attr,$value){
		$this->buttonAttr[$attr] = $value;
		return $this;
	}
	public function getButtonAttr(){
		$attrs="";
		foreach($this->buttonAttr as $attr => $value){
			$attrs.=' '.$attr.'="'.$value.'" ';
		}
		return $attrs;
	}
	public function getItemAttr(){
		$this->checkGeneratingId();
		if($this->button == null){
			$loader ="";
			if($this->loader !=""){
				$loader = ",'$this->loader'";
			}
			$attr = $this->event."=\"javascript:ajax_load('$this->inputId','$this->function','$this->responseDiv',this.value,$this->checkAfter,".(int)$this->noScroll."".$loader.");\"";
		} else {
			$attr = "onkeyup=\"javascript:redirectText(this.value,'$this->inputId');\"";
			$attr .= " onchange=\"javascript:redirectText(this.value,'$this->inputId');\"";
		}
		return $attr;
	}
	public function getAjaxHtml(){
		$this->checkGeneratingId();
		if($this->button != null){
			$b=" <a id=\"CHECK_$this->inputId\" ".$this->getButtonAttr()." href=\"javascript:ajax_load('$this->inputId','$this->function','$this->responseDiv',getRelValue('$this->inputId'),$this->checkAfter,".(int)$this->noScroll.");\">$this->button</a>";
			//$b=" <a href=\"javascript:;\" onclick=\"javascript:ajax_load('$this->inputId','$this->function','$this->responseDiv','');\">$this->button</a>";
			//$b=" <a href=\"javascript:ajax_load('$this->inputId','$this->function','$this->responseDiv',document.getElementById('$this->inputId').value);\">$this->button</a>";
			return $b;
		}
		return "";
	}
}