<?php
namespace Kubomikita;
class Format {
	static function shortFilename($string,$max_length,$end_substitute="..."){
		if(strlen($string) >= $max_length){
			return "<span title=\"$string\">".mb_substr($string, 0, $max_length, 'UTF-8').$end_substitute."</span>";
		} else {
			return $string;
		}
	}

	static function shortText($string, $max_length, $end_substitute = "...", $html_linebreaks = true) {

		if($html_linebreaks) $string = preg_replace('/\<br(\s*)?\/?\>/i', "\n", $string);
		$string = strip_tags($string); //gets rid of the HTML

		if(empty($string) || mb_strlen($string) <= $max_length) {
			if($html_linebreaks) $string = nl2br($string);
			return $string;
		}

		if($end_substitute) $max_length -= mb_strlen($end_substitute, 'UTF-8');

		$stack_count = 0;
		while($max_length > 0){
			$char = mb_substr($string, --$max_length, 1, 'UTF-8');
			if(preg_match('#[^\p{L}\p{N}]#iu', $char)) $stack_count++; //only alnum characters
			elseif($stack_count > 0) {
				$max_length++;
				break;
			}
		}
		$string = mb_substr($string, 0, $max_length, 'UTF-8').$end_substitute;
		if($html_linebreaks) $string = nl2br($string);

		return $string;
	}
	static function date($date,$pattern="d.m.Y H:i:s"){
		if(!is_int($date)){
			$date=strtotime($date);
		}
		return date($pattern,$date);
	}
	static function chmod($perms){
		if (($perms & 0xC000) == 0xC000) {
			// Socket
			$info = 's';
		} elseif (($perms & 0xA000) == 0xA000) {
			// Symbolic Link
			$info = 'l';
		} elseif (($perms & 0x8000) == 0x8000) {
			// Regular
			$info = '-';
		} elseif (($perms & 0x6000) == 0x6000) {
			// Block special
			$info = 'b';
		} elseif (($perms & 0x4000) == 0x4000) {
			// Directory
			$info = 'd';
		} elseif (($perms & 0x2000) == 0x2000) {
			// Character special
			$info = 'c';
		} elseif (($perms & 0x1000) == 0x1000) {
			// FIFO pipe
			$info = 'p';
		} else {
			// Unknown
			$info = 'u';
		}

		// Owner
		$info .= (($perms & 0x0100) ? 'r' : '-');
		$info .= (($perms & 0x0080) ? 'w' : '-');
		$info .= (($perms & 0x0040) ?
			(($perms & 0x0800) ? 's' : 'x' ) :
			(($perms & 0x0800) ? 'S' : '-'));

		// Group
		$info .= (($perms & 0x0020) ? 'r' : '-');
		$info .= (($perms & 0x0010) ? 'w' : '-');
		$info .= (($perms & 0x0008) ?
			(($perms & 0x0400) ? 's' : 'x' ) :
			(($perms & 0x0400) ? 'S' : '-'));

		// World
		$info .= (($perms & 0x0004) ? 'r' : '-');
		$info .= (($perms & 0x0002) ? 'w' : '-');
		$info .= (($perms & 0x0001) ?
			(($perms & 0x0200) ? 't' : 'x' ) :
			(($perms & 0x0200) ? 'T' : '-'));

		return $info;
	}
	public static function seoName($nadpis) {
		return \Nette\Utils\Strings::webalize($nadpis);
	}
	static function convertSecToStr($secs){
		$output = '';
		if($secs >= 86400) {
			$days = floor($secs/86400);
			$secs = $secs%86400;
			$output = $days.' dní';
			if($days != 1) $output .= '';
			if($secs > 0) $output .= ', ';
		}
		if($secs>=3600){
			$hours = floor($secs/3600);
			$secs = $secs%3600;
			$output .= $hours.' hodín';
			if($hours != 1) $output .= '';
			if($secs > 0) $output .= ', ';
		}
		if($secs>=60){
			$minutes = floor($secs/60);
			$secs = $secs%60;
			$output .= $minutes.' minút';
			/*if($minutes != 1) $output .= '';
			if($secs > 0) $output .= ', ';*/
		}
		/*$output .= $secs.' second';
		if($secs != 1) $output .= '';*/
		return $output;
	}
}
