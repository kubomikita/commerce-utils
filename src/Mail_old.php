<?php
class Mail_old {
	public $from;
	public $to;
	public $subject;
	public $text;
	public $bcc;
	public $cc;
	private $attachments = [];
	public $content_type='text/plain';
	public $EOL = "\n";
	private $mime_boundary = '';

	private function mime_boundary(){
		if($this->mime_boundary==''){ $this->mime_boundary=md5(uniqid(time())); };
		return $this->mime_boundary;
	}

	private function build($part=''){
		mb_internal_encoding('UTF-8');
		$uid = $this->mime_boundary();
		if(strstr($this->from,'<')){
			$from=mb_encode_mimeheader(substr($this->from,0,strpos($this->from,'<')), "UTF-8", "Q").' '.substr($this->from,strpos($this->from,'<'));
		} else {
			$from=$this->from;
		};
		$header = "From: ".$from.$this->EOL;
		if($this->cc!=''){ $header .= "Cc: ".$this->cc.$this->EOL; };
		if($this->bcc!=''){ $header .= "Bcc: ".$this->bcc.$this->EOL; };
		//$header .= "Reply-To: ".$replyto."\r\n";
		$header .= "MIME-Version: 1.0".$this->EOL;
		$header .= "Content-Type: ".(!empty($this->attachments)?"multipart/mixed":"multipart/related")."; boundary=\"".$uid."\"".$this->EOL;
		$header .= "This is a multi-part message in MIME format.";

		$body = "--".$uid.$this->EOL;

		$body .= "Content-Type: ".$this->content_type."; charset=utf-8".$this->EOL;
		//$header .= "Content-type:text/plain; charset=utf-8\r\n";
		$body .= "Content-Transfer-Encoding: 8bit".$this->EOL.$this->EOL;
		$body .= $this->text.$this->EOL;
		//$header .= iconv('cp1250','UTF-8',$_REQUEST['text'])."\r\n\r\n";
		$body .= "--".$uid."";

		if(!empty($this->attachments)){foreach($this->attachments as $attach){
			$content = chunk_split(base64_encode($attach['content']));

			$body .= $this->EOL;
			$body .= "Content-Type: ".$attach['mimetype']."; name=\"".$attach['filename']."\"".$this->EOL; // use diff. tyoes here
			$body .= "Content-Transfer-Encoding: base64".$this->EOL;
			$body .= "Content-Disposition: attachment; filename=\"".$attach['filename']."\"".$this->EOL.$this->EOL;
			$body .= $content.$this->EOL;
			$body .= "--".$uid."";
		};};


		$body .= "--".$this->EOL.$this->EOL;
		$ret=array('header'=>$header,'body'=>$body);
		return $ret[$part];
	}

	private function build_old(){
		mb_internal_encoding('UTF-8');
		$uid = md5(uniqid(time()));
		if(strstr($this->from,'<')){
			$from=mb_encode_mimeheader(substr($this->from,0,strpos($this->from,'<')), "UTF-8", "Q").' '.substr($this->from,strpos($this->from,'<'));
		} else {
			$from=$this->from;
		};
		$header = "From: ".$from.$this->EOL;
		if($this->bcc!=''){ $header .= "Bcc: ".$this->bcc.$this->EOL; };
		//$header .= "Reply-To: ".$replyto."\r\n";
		$header .= "MIME-Version: 1.0".$this->EOL;
		$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"".$this->EOL;
		$header .= "This is a multi-part message in MIME format.".$this->EOL.$this->EOL;
		$header .= "--".$uid.$this->EOL;
		$header .= "Content-type:".$this->content_type."; charset=utf-8".$this->EOL;
		//$header .= "Content-type:text/plain; charset=utf-8\r\n";
		$header .= "Content-Transfer-Encoding: 8bit".$this->EOL.$this->EOL;
		$header .= $this->text.$this->EOL.$this->EOL;
		//$header .= iconv('cp1250','UTF-8',$_REQUEST['text'])."\r\n\r\n";
		$header .= "--".$uid."";

		if(sizeof($this->attachments)){foreach($this->attachments as $attach){
			$content = chunk_split(base64_encode($attach['content']));

			$header .= $this->EOL;
			$header .= "Content-Type: ".$attach['mimetype']."; name=\"".$attach['filename']."\"".$this->EOL; // use diff. tyoes here
			$header .= "Content-Transfer-Encoding: base64".$this->EOL;
			$header .= "Content-Disposition: attachment; filename=\"".$attach['filename']."\"".$this->EOL.$this->EOL;
			$header .= $content.$this->EOL.$this->EOL;
			$header .= "--".$uid."";
		};};


		$header .= "--";
		return $header;
	}

	public function attach($filename,$content){
		$mime='application/octet-stream';
		if(func_num_args()>=3){ $mime=func_get_arg(2); };
		$this->attachments[]=array('filename'=>$filename,'content'=>$content,'mimetype'=>$mime);
	}

	public function send(){
		mb_internal_encoding('UTF-8');
		$this->html_safe_process();
		//mail($this->to, strtr(mb_encode_mimeheader($this->subject, "UTF-8", "Q"),array("\n"=>'',"\r"=>'')), "", $this->build());
		return mail($this->to, strtr(mb_encode_mimeheader($this->subject, "UTF-8", "Q"),array("\n"=>'',"\r"=>'')), $this->build('body'), $this->build('header'), "-f ".$this->to);
	}

	private function html_safe_process(){
		$do=0;
		if(strstr($this->to,'azet.sk')){ $do=1; };
		if($do){
			//$this->attach("_.html",$this->text);
		};
	}


	public function multisend(){
		mb_internal_encoding('UTF-8');
		//$mail=$this->build();
		$mail_head=$this->build('header');
		$mail_body=$this->build('body');
		foreach(explode(',',$this->to) as $to){
			//mail(trim($to), strtr(mb_encode_mimeheader($this->subject, "UTF-8", "Q"),array("\n"=>'',"\r"=>'')), "", $mail);
			mail(trim($to), strtr(mb_encode_mimeheader($this->subject, "UTF-8", "Q"),array("\n"=>'',"\r"=>'')), $mail_body, $mail_head);
		};
	}

}









?>
