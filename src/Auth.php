<?php

class Auth {
	static public $userId;
	static public $userName;
	static public $auth = 0;
	/** @var  Partner */
	static public $User;
	static public $mainPage;
	static public $identity;

	static public function ACL($priv){
		$ret=0;
		if(is_object(Auth::$User) && method_exists(Auth::$User,'ACL')){ $ret=Auth::$User->ACL($priv); };
		return (int)$ret;
	}

	public static function User(){
		return (Auth::$User instanceof Partner)?(Auth::$User->id>0):null;
	}
	public static function Admin(){
		$ret=false;
		if(method_exists(Auth::$User,'admin')){ $ret=(Auth::$User->admin()); };
		return $ret;
	}

	public static function checkUser($U){
		return (Auth::$User->id==$U->id);
	}
	public static function checkUserOrAdmin($U){
		return (Auth::$User->id==$U->id || Auth::Admin());
	}
	public static function init(){

	}

}
