<?php

namespace Kubomikita\Commerce;

use Nette\Mail\IMailer;
use Nette\Mail\Message;
use Nette\Mail\SendException;

class DevNullMailer implements IMailer{
	public function send( Message $mail ) :void {
		throw new SendException("DevNullMailer is setted. <strong>Message wasn't sent.</strong>");
	}
}