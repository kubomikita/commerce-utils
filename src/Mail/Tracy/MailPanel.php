<?php
namespace Kubomikita\Commerce;

use Tracy\Helpers;
use Tracy\IBarPanel;
final class MailPanel implements IBarPanel {
	/** @var MailService */
	private $MailService;

	public function __construct(MailService $service) {
		$this->MailService = $service;
	}

	/**
	 * Renders HTML code for custom tab.
	 */
	public function getTab(): string
	{
		$mailsCount = count($this->MailService->getMails());
		if ($mailsCount === 0) {
			return '';
		}
		ob_start();
		require 'templates/tab.phtml';
		return (string) ob_get_clean();
	}
	/**
	 * Renders HTML code for custom panel.
	 */
	public function getPanel(): string
	{
		$mails = $this->MailService->getMails();
		if(!Helpers::isAjax()) {
			$this->MailService->flushMails();
		}
		ob_start();
		require 'templates/panel.phtml';
		return (string) ob_get_clean();
	}
}