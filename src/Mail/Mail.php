<?php

use Kubomikita\Service;
use Kubomikita\Commerce\MailService;
use Nette\Mail\Message;

/**
 * @property-write $to
 * @property-write $subject
 * @property-write $text
 * @property-write $cc
 * @property-write $bcc
 * @property-write $replyTo
 */
class Mail {
	/** @deprecated Please use method setFrom($email,$name=null) */
	public $from;
	/** @deprecated  */
	//public $to;
	/** @deprecated  */
	//public $subject;
	/** @deprecated  */
	//public $text;
	/** @deprecated  */
	//public $bcc;
	/** @deprecated  */
	//public $cc;
	/** @deprecated array  */
	//private $attachments = [];
	/** @deprecated  */
	public $content_type='text/plain';
	/** @deprecated  */
	public $EOL = "\n";
	/** @deprecated  */
	private $mime_boundary = '';

	/** @var MailService */
	private $MailService;
	/** @var Message */
	private $message;
	private $propertyToFunction = ["to" => "addTo","subject"=>"setSubject","text"=>"setBody","bcc"=>"addBcc","cc"=>"addCc","replyTo"=>"addReplyTo"];
	private $data = [];

	private $defaultFromName;
	private $defaultFromMail;

	public function __construct() {
		$this->MailService = Service::get("mail");
		$this->defaultFromName = Service::get("container")->getParameter("shop","name");
		$this->defaultFromMail = Service::get("container")->getParameter("shop","email");
		$this->message = new Message();
		$this->message->setFrom($this->defaultFromMail,$this->defaultFromName)
						->setReturnPath($this->defaultFromMail);
		$this->data["from"]["email"] = $this->defaultFromMail;
		$this->data["from"]["name"] = $this->defaultFromName;
		$this->data["return-path"] = $this->defaultFromMail;
	}

	/**
	 * @param $name
	 * @param $value
	 */
	public function __set( $name, $value ) {
		if(isset($this->propertyToFunction[$name])){
			$this->{$this->propertyToFunction[$name]}($value);
		}
	}

	/**
	 * @deprecated
	 * @return string
	 */
	private function mime_boundary(){
		if($this->mime_boundary==''){ $this->mime_boundary=md5(uniqid(time())); };
		return $this->mime_boundary;
	}

	/**
	 * @deprecated
	 * @param string $part
	 *
	 * @return mixed
	 */
	private function build($part=''){
		mb_internal_encoding('UTF-8');
		$uid = $this->mime_boundary();
		if(strstr($this->from,'<')){
			$from=mb_encode_mimeheader(substr($this->from,0,strpos($this->from,'<')), "UTF-8", "Q").' '.substr($this->from,strpos($this->from,'<'));
		} else {
			$from=$this->from;
		};
		$header = "From: ".$from.$this->EOL;
		if($this->cc!=''){ $header .= "Cc: ".$this->cc.$this->EOL; };
		if($this->bcc!=''){ $header .= "Bcc: ".$this->bcc.$this->EOL; };
		//$header .= "Reply-To: ".$replyto."\r\n";
		$header .= "MIME-Version: 1.0".$this->EOL;
		$header .= "Content-Type: ".(!empty($this->attachments)?"multipart/mixed":"multipart/related")."; boundary=\"".$uid."\"".$this->EOL;
		$header .= "This is a multi-part message in MIME format.";

		$body = "--".$uid.$this->EOL;

		$body .= "Content-Type: ".$this->content_type."; charset=utf-8".$this->EOL;
		//$header .= "Content-type:text/plain; charset=utf-8\r\n";
		$body .= "Content-Transfer-Encoding: 8bit".$this->EOL.$this->EOL;
		$body .= $this->text.$this->EOL;
		//$header .= iconv('cp1250','UTF-8',$_REQUEST['text'])."\r\n\r\n";
		$body .= "--".$uid."";

		if(!empty($this->attachments)){foreach($this->attachments as $attach){
			$content = chunk_split(base64_encode($attach['content']));

			$body .= $this->EOL;
			$body .= "Content-Type: ".$attach['mimetype']."; name=\"".$attach['filename']."\"".$this->EOL; // use diff. tyoes here
			$body .= "Content-Transfer-Encoding: base64".$this->EOL;
			$body .= "Content-Disposition: attachment; filename=\"".$attach['filename']."\"".$this->EOL.$this->EOL;
			$body .= $content.$this->EOL;
			$body .= "--".$uid."";
		};};


		$body .= "--".$this->EOL.$this->EOL;
		$ret=array('header'=>$header,'body'=>$body);
		return $ret[$part];
	}

	/**
	 * @deprecated
	 * @param $filename
	 * @param $content
	 */
	public function attach($filename,$content){
		$this->addAttachment($filename,$content);
	}

	/**
	 * @param $filename
	 * @param $content
	 *
	 * @return $this
	 */
	public function addAttachment($filename,$content){
		$this->message->addAttachment($filename,$content);
		$this->data["attachments"][] = [$filename,$content];
		return $this;
	}

	/**
	 * @param $email
	 * @param null $name
	 */
	public function setFrom($email,$name=null){
		$this->message->setFrom($email,$name);
		$this->data["from"]["email"] = $email;
		$this->data["from"]["name"] = $name;
	}
	/**
	 * @param $email
	 *
	 * @return $this
	 */
	public function addTo($email){
		$email = trim($email);
		$this->message->addTo($email);
		$this->data["to"][] = $email;
		return $this;
	}
	public function addReplyTo($email){
		$this->message->addReplyTo($email);
		$this->data["replyTo"] = $email;
		return $this;
	}

	/**
	 * @param $email
	 *
	 * @return $this
	 */
	public function addBcc($email){
		$this->message->addBcc($email);
		$this->data["bcc"][] = $email;
		return $this;
	}

	/**
	 * @param $email
	 *
	 * @return $this
	 */
	public function addCc($email){
		$this->message->addCc($email);
		$this->data["cc"][] = $email;
		return $this;
	}

	/**
	 * @param $subject
	 *
	 * @return $this
	 */
	public function setSubject($subject){
		$this->message->setSubject($subject);
		$this->data["subject"] = $subject;
		return $this;
	}

	/**
	 * @param $body
	 *
	 * @return $this
	 */
	public function setBody($body){
		$this->message->setHtmlBody($body);
		$this->data["body"] = $body;
		return $this;
	}
	private function beforeSend(){
		if(isset($this->MailService->options["username"]) && $this->MailService->options["username"] != $this->data["from"]["email"]){
			$newemail = $this->MailService->options["username"];
			$this->data["from"]["email"] = $newemail;
			$this->message->setFrom($this->data["from"]["email"],$this->data["from"]["name"]);
		}
	}
	/**
	 * Sends email via config based mailer
	 */
	public function send(){
		$mailerClass = get_class($this->MailService->mailer);
		if(\Nette\Utils\Strings::contains($mailerClass,"SendmailMailer")){
			$this->MailService->mailer->commandArgs="-f".$this->data["from"]["email"];
		}
		$this->beforeSend();
		try {
			$this->MailService->mailer->send( $this->message );
			$this->data["sended"] = true;
			$this->log();
		} catch (\Nette\Mail\SendException | \Nette\Mail\SmtpException  $e) {
			Tracy\Debugger::log( $e->getMessage(), Tracy\Debugger::EXCEPTION );
			$this->data["sended"] = false;
			$this->log( $e->getMessage() );
		}
	}
	private function log($message = null){
		//if($this->MailService->context->isDebugMode()) {
			if($message !== null){
				$this->data["error"] = $message;
			}
			$this->MailService->setMail(["message" => $this->message, "data" => $this->data]);
		//}
	}
	/**
	 * @deprecated
	 * @return bool
	 */
	public function send_old(){
		mb_internal_encoding('UTF-8');
		$this->html_safe_process();
		//echo('<textarea>'.$this->build('header').'</textarea><pre>'.$this->build('body').'</pre><pre>'.strtr(mb_encode_mimeheader($this->subject, "UTF-8", "Q"),array("\n"=>'',"\r"=>'')).'</pre>');
		//mail($this->to, strtr(mb_encode_mimeheader($this->subject, "UTF-8", "Q"),array("\n"=>'',"\r"=>'')), "", $this->build());
		return mail($this->to, strtr(mb_encode_mimeheader($this->subject, "UTF-8", "Q"),array("\n"=>'',"\r"=>'')), $this->build('body'), $this->build('header'), "-f ".$this->defaultFromMail);
	}

	/**
	 * @deprecated
	 */
	private function html_safe_process(){
		$do=0;
		if(strstr($this->to,'azet.sk')){ $do=1; };
		if($do){
			//$this->attach("_.html",$this->text);
		};
	}



}









?>
