<?php
namespace Kubomikita\Image;
use Nette\InvalidArgumentException;
use Nette\Utils\FileSystem;
use Nette\Utils\Finder;
use Nette\Utils\ImageException;
use Nette\Utils\Strings,
	Nette\Utils\Image,
	Nette\Database\Connection;
use Tracy\Debugger;

class ImgCacheHandler {
	const TYPE_CATEGORY = 'kat';
	const TYPE_PRODUCT = 'item';
	const TYPE_BRAND = 'vyrobca';
	const TYPE_STORE = 'predajna';
	const TYPE_BLANK = 'blank';
	const TYPES = [

	];

	const WATERMARK_OVER = 1;
	const WATERMARK_RIGHT_BOTTOM = 2;

	/** @var Connection */
	protected $db;

	protected $class;
	protected $id;
	protected $format;
	protected $resizeType;
	protected $sx;
	protected $sy;
	protected $color;

	/** @var Image|null */
	protected $image;
	/** @var int */
	protected $imageType = Image::JPEG;

	private $useFileStorage = \Config::useImgFileStore;
	private $saveImage = true;
	/** @var string */
	protected $watermark;
	protected $watermarkAlpha = 100;
	protected $watermarkMargin = 50;
	protected $watermarkPosition = self::WATERMARK_OVER;
	protected $watermarkDisableType = [];

	public function __construct(array $request, Connection $connection){
		$this->db = $connection;

		$this->class = !isset($request["class"]) ? null : $request["class"];
		$this->id = !isset($request["id"]) ? null : (int) $request["id"];
		$this->format = !isset($request["format"]) ? "jpg" : $request["format"];
		$this->resizeType = !isset($request["resize"]) ? null : (int) $request["resize"];
		$this->sx = !isset($request["sx"]) ? null : (int) $request["sx"];
		$this->sy = !isset($request["sy"]) ? null : (int) $request["sy"];
		$this->color = isset($request["color"]) && strlen($request["color"]) > 0 ?  (string) $request["color"] : null;

		if($this->id === null){
			throw new \Nette\InvalidArgumentException("Parameter 'id' of image missing.");
		}
	}

	protected function getResourceByType(){
		$COLUMN='img';
		switch($this->class){
			case "news":
				$QUERY="SELECT img FROM cms_news_data WHERE id = ?";
				break;
			case "news_title":
				$QUERY="SELECT img_title FROM cms_news_data WHERE id = ? ";
				$COLUMN='img_title';
				break;
			case "img":
				$QUERY="SELECT img FROM ".\Config::tableImgImages." WHERE id= ?";
				if($this->useFileStorage && file_exists(WWW_DIR."bindata/ei-".$this->id.".jpg")){
					$QUERY=null;
					$FILE=WWW_DIR.'bindata/ei-'.$this->id.'.jpg';
				}
				break;
			case "cms":
				$QUERY="SELECT img FROM cms_img_images WHERE id = ?";
				if($this->useFileStorage && file_exists(WWW_DIR."bindata/cms-".$this->id.".jpg")){
					$QUERY=null;
					$FILE=WWW_DIR.'bindata/cms-'.$this->id.'.jpg';
				}
				break;
			case self::TYPE_CATEGORY:
				if($this->useFileStorage){
					$FILE = WWW_DIR.'bindata/ek-'.$this->id.'.jpg';
				}
				break;
			case self::TYPE_PRODUCT:
				if($this->useFileStorage){
					$FILE = WWW_DIR.'bindata/et-'.$this->id.'.jpg';
				}
				break;
			case self::TYPE_BRAND:
				if($this->useFileStorage){
					$FILE = WWW_DIR.'bindata/ev-'.$this->id.'.jpg';
				}
				break;
			case "gift":
				if($this->useFileStorage){
					$FILE = WWW_DIR.'bindata/ed-'.$this->id.'.jpg';
				}
				break;
			case self::TYPE_STORE:
				if($this->useFileStorage){
					$FILE = WWW_DIR.'bindata/predajna-'.$this->id.'.jpg';
				}
				break;
			case self::TYPE_BLANK:
				if($this->useFileStorage){
					$FILE = \BlankImg::storage_file();
				}
				break;
		}
		//dumpe($FILE,$QUERY,$COLUMN);
		if( isset($FILE) ) {
			try {
				$this->image     = Image::fromFile( $FILE );
				$this->imageType = exif_imagetype( $FILE );
			} catch ( \Throwable $e ) {
				trigger_error( $e->getMessage() );
				$this->blankImage();
			}
		} elseif (isset($QUERY) && isset($COLUMN)) {
			$r = $this->db->query($QUERY, $this->id)->fetch();
			//dumpe($r);
			if($r && $r[$COLUMN] !== null){
				$this->image = Image::fromString($r[$COLUMN]);
			} else {
				$this->blankImage();
			}
		} else {
			$this->blankImage();
		}
	}

	protected function getBackground(){
		if($this->color === null || $this->color == "transparent") {
			if ( $this->imageType == Image::PNG ) {
				$white = Image::rgb( 255, 255, 255, 127 );
			} else {
				$white = Image::rgb( 255, 255, 255 );
			}
		} else {
			$color = hexdec( $this->color );
			$CB    = ( $color % 256 );
			$CG    = ( floor( $color / 256 ) % 256 );
			$CR    = ( floor( $color / 65536 ) % 256 );
			$white = Image::rgb($CR,$CG,$CB);
		}
		return $white;
	}

	protected function processResize(){
		if($this->resizeType !== null){
			if( !($this->sx !== null || $this->sy !== null)){
				throw new InvalidArgumentException("Missing parameters 'sx' or 'sy' required for resize.");
			}
			switch($this->resizeType) {
				case 1:
					$this->image->resize($this->sx,$this->sy,Image::STRETCH);
					break;
				case 2:

					break;
				case 3:
					$im = Image::fromBlank($this->sx,$this->sy,$this->getBackground());
					$im->place($this->image->resize($this->sx,$this->sy,Image::SHRINK_ONLY | Image::FIT),"50%","50%");
					$this->image = $im;
					break;
				case 4:
					$this->image->resize(null,$this->sy,Image::SHRINK_ONLY | Image::FIT);
					break;
				case 5:
					$img = $this->image;
					$width=$this->image->getWidth();
					$height=$this->image->getHeight();

					$thumb_width = $this->sx;
					$thumb_height = $this->sy;

					$original_aspect = $width / $height;
					$thumb_aspect = $thumb_width / $thumb_height;
					if ( $original_aspect >= $thumb_aspect ){
						$new_height = (int) $thumb_height;
						$new_width = (int) ($width / ($height / $thumb_height));
						$top = 0;
						$left = (int) (($new_width - $thumb_width) / 2);
					} else {
						$new_width = (int) $thumb_width;
						$new_height = (int) ($height / ($width / $thumb_width));
						$left = 0;
						$top = (int) (($new_height - $thumb_height) / 2);
					}

					$img->resize($new_width,$new_height);
					$img->sharpen();
					$img->crop($left,$top,$new_width,$new_height);
					$im = Image::fromBlank($thumb_width,$thumb_height,$this->getBackground());
					$im->place($img);
					$this->image = $im;
					break;
			}
		} else {
			$im = Image::fromBlank($this->image->getWidth(), $this->image->getHeight(), $this->getBackground());
			$im->place($this->image);
			$this->image = $im;
		}
	}

	protected function saveCache(){
		if(Strings::contains($_SERVER["REQUEST_URI"],"imgcache") && !Strings::contains($_SERVER["REQUEST_URI"],".php") && $this->saveImage) {
			$file = basename($_SERVER['REQUEST_URI']);
			$file = parse_url($file);
			$filename = $file["path"];
			$this->image->save( WWW_DIR . "imgcache/" . $filename , null, $this->imageType);
		}
	}

	protected function createWebp(){
		if($this->format === "webp" && class_exists(\WebPConvert\WebPConvert::class)){
			$filewebp = basename($_SERVER['REQUEST_URI']);
			$filewebp = parse_url($filewebp);
			$filewebp = $filewebp["path"];

			$img = $this->image;

			$dir = APP_DIR."temp/webp/";
			if(!file_exists($dir)){
				\Nette\Utils\FileSystem::createDir($dir);
			}
			$filename = $dir.md5(serialize($_REQUEST)).".jpg";
			$img->save($filename);

			try {
				$success = \WebPConvert\WebPConvert::convertAndServe( $filename, WWW_DIR . "imgcache/" . $filewebp, [
					'fail' => 'original',
					'converters' => [ 'cwebp', 'gd', 'imagick' ],
					"aboutToServeImageCallBack" => function() use ($filename) {
						\Nette\Utils\FileSystem::delete($filename);
					}
				] );

			} catch (\WebPConvert\Exceptions\InvalidFileExtensionException $e){
				trigger_error($e->getMessage());
			}
			exit;
		}
	}

	protected function blankImage(){
		$this->image = Image::fromFile(\BlankImg::storage_file());
		$this->imageType = exif_imagetype( \BlankImg::storage_file() );
		$this->saveImage = false;
	}

	public function addWatermark(string $file, int $position = self::WATERMARK_OVER, int $alpha = 100 ,int $margin = 90, $disableType = [] ) : self {
		$this->watermark = $file;
		$this->watermarkAlpha = $alpha;
		$this->watermarkMargin = $margin;
		$this->watermarkPosition = $position;
		$this->watermarkDisableType = $disableType;
		return $this;
	}
	protected function setWatermark() {
		if($this->watermark !== null && !in_array($this->class, $this->watermarkDisableType)) {
			$watermark = Image::fromFile($this->watermark);
			if($this->watermarkPosition === self::WATERMARK_OVER) {
				$wmx = $watermark->getWidth() + $this->watermarkMargin;
				$wmy = $watermark->getHeight() + $this->watermarkMargin;
				for ( $x = 0; $x < ceil( $this->image->getWidth() / $wmx ); $x ++ ) {
					$px = $x * $wmx;
					for ( $y = 0; $y < ceil( $this->image->getHeight() / $wmy ); $y ++ ) {
						$py = $y * $wmy;
						$this->image->place( $watermark, $px, $py, $this->watermarkAlpha );
					}
				}
			} elseif ($this->watermarkPosition === self::WATERMARK_RIGHT_BOTTOM) {
				$this->image->place( $watermark, $this->watermarkMargin."%", $this->watermarkMargin."%", $this->watermarkAlpha );
			}
		}
	}

	public function flushCache() {
		foreach (Finder::find(["*.jpg","*.png",".webp"])->in(WWW_DIR."/imgcache") as $img ){
			try {
				FileSystem::delete( $img );
			} catch (\Throwable $e) {}
		}
	}

	public function send(){
		Debugger::$showBar = false;
		$this->getResourceByType();
		$this->processResize();
		$this->setWatermark();
		$this->createWebp();
		$this->saveCache();
		$this->image->send($this->imageType);
	}

	/**
	 * @param bool $saveImage
	 */
	public function setSaveImage( bool $saveImage ): self {
		if(!$saveImage) {
			$this->flushCache();
		}
		$this->saveImage = $saveImage;
		return $this;
	}
}