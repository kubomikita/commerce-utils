<?php
class LangStr {
	private $map;
	static public $locale = 'sk';
	static public $native = 'sk';
	static public $document_locale = 'sk';
	static private $Map;
	const binary_separator = '☻'; // :)

	static private function parse($in){
		$v=array();
		if($in instanceof LangStr){
			$v=$in->map;
		} else if(is_array($in)){
			$v=$in;
		} else {
			if(substr($in,0,1)=='{'){
				$v=json_decode($in,1);
			} else if(mb_substr($in,0,1)==LangStr::binary_separator){
				foreach(explode(LangStr::binary_separator,$in) as $t){
					if(mb_strlen($t)){
						$key=substr($t, 0, 2);
						$val=substr($t, 3);
						$v[$key]=$val;
					};
				};
			} else {
				$v[LangStr::$native]=$in;
			};
		};
		return $v;
	}

	static public function loadMap($m){
		if(is_array($m)){
			foreach($m as $k=>$in){
				LangStr::$Map[$k]=LangStr::parse($in);
			}
		}
	}

	static public function dumpMap(){
		return LangStr::$Map;
	}

	static public function getLang(){
		if(LangStr::$locale!=''){ $ret=LangStr::$locale; } else { $ret=LangStr::$native; };
		return $ret;
	}

	function __construct($in=null){
		if($in === null){
			$in = "";
		}
		$Map=LangStr::dumpMap();
		if($in instanceof LangStr){
			while($in->map[LangStr::$native] instanceof LangStr){ $in->map=$in->map[LangStr::$native]->map; }; // ojebavka na zacyklenie
			$this->map=$in->map;
		} else if(@isset($Map[$in]) and is_string($in) && is_array($Map[$in])){
			$this->map=$Map[$in];
		} else {
			$this->map=LangStr::parse($in);
		};
	}

	function __tostring(){
		//bdump($this->map[LangStr::$native],isset($this->map[LangStr::$locale]));
		if(isset($this->map[LangStr::$locale]) && $this->map[LangStr::$locale] != ""){
			return (string)$this->map[LangStr::$locale];
		} else {
			return (string)$this->map[LangStr::$native];
		}
	}

	public function json(){
		return json_encode($this->map);
	}

	public function assoc(){
		return $this->map;
	}

	public function lang($lang){
		return $this->map[$lang];
	}

	public function bstring(){
		$ret='';
		foreach($this->map as $k=>$v){
			$ret.=LangStr::binary_separator.$k.':'.$v.LangStr::binary_separator;
		};
		return $ret;

	}

	static public function sqlExtract($input,$lang){
		return "substr(".$input.",locate('".LangStr::binary_separator."".$lang.":',".$input.")+4,locate('".LangStr::binary_separator."',substr(".$input.",locate('".LangStr::binary_separator."".$lang.":',".$input.")+4))-1)";
	}

}

function LangStr($str){
	$L=new LangStr($str);
	return (string)$L;
}

if(!function_exists('__')){
	//function __($str){
	/*$translator = Registry::get("translator");
	if($translator !== null){
		$ret = $translator->gettext($str);
		return $ret;
	}
	$L=new LangStr($str);*/
	/* $L=new LangStr(str_replace(array("|","•","!","?","&","(",")","[","]","*","'","\"","+","/","<",">"),array("__","_-_","-_-",".-.","aanndd","l_zatv","p_zatv","l_lzatv","p_lzatv","_star-","_quote-","_doublequote-","_plus-","_lomitko-","_lsipka-","_psipka-"),trim($str)));
	 if((String) $L != ""){
		 if(LangStr::$locale=="sk"){
			 $file = file_get_contents(Config::ROOT."preklad.txt");
			 $addrow = str_replace(array("|","•","!","?","&","(",")","[","]","*","'","\"","+","/","<",">"),array("__","_-_","-_-",".-.","aanndd","l_zatv","p_zatv","l_lzatv","p_lzatv","_star-","_quote-","_doublequote-","_plus-","_lomitko-","_lsipka-","_psipka-"),trim($str))." = \"".trim($str)."\"\n";
			 if(strpos($file,$addrow) === false){
				 file_put_contents(Config::ROOT."preklad.txt",$file.$addrow);
			 }

		 }
	 }*/
	/*    bdump($L);
        return (string) $L;
    }*/
}

if(!function_exists('_')){
	function _($str){
		$L=new LangStr($str);
		return (String) $L;
	}
}















?>
