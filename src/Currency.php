<?php
use  \Nette\Caching;
class Currency {

	public static $native = 'EUR';

	public static function rate_ecb_get($curr,$timestamp=0){
		if($curr==Currency::$native){ 
			return 1; 
		}
		if($timestamp>0){ 
			$Date=new Timestamp((int)$timestamp); 
		} else { 
			$Date=new Timestamp(); 
		}

		$content = HTTP::get("https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml");

		if($content==''){ return false; };
		$xml = simplexml_load_string($content);
		utf8_decode($xml);

		//list($dd,$dm,$dy)=explode('.',$R['datum_vzniku_dp']);
		$dnow=($Date->show('Y')*10000)+($Date->show('m')*100)+($Date->show('d'));

		$ddx=0;

		foreach($xml->Cube->Cube as $DD){
			$date=(string)$DD['time'];
			list($dy,$dm,$dd)=explode('-',$date);
			$ddat=($dy*10000)+($dm*100)+($dd);
			$xData[$ddat]=$DD;
			if($ddat>$ddx && $ddat<=$dnow){ $ddx=$ddat;};
		};

		foreach($xData[$ddx]->Cube as $Curr){
			$currency=(string)$Curr['currency'];
			$rate=(string)$Curr['rate'];
			if($currency==$curr){ $convrate=$rate; };
		};

		return (float) $convrate;
	}
	public static function rate_fio_get($curr,$timestamp=0){
		if($curr==Currency::$native){ return 1; };
		//if($curr!="CZK"){ return 1; };
		//$content = HTTP::get("http://www.fio.sk/akcie-investicie/dalsie-sluzby-fio/devizove-konverzie");
		$content = HTTP::get("https://www.fio.sk/akcie-investicie/dalsie-sluzby-fio/devizove-konverzie");
		echo($content);
		if($content==""){return false;}
		$re = "/(".$curr.")<\\/td>\\s*<td.*<\\/td>\\s*<.*>\\s*<td class=\"tright\">(.*)<\\/td><td class=\"tright\">(.*)<\\/td>/m";
		$listok = preg_match($re,$content,$matches);
		unset($matches[0]);
		if(!$listok) return null;
		//dump($matches);
		$kurz=array();
		$kurz["currency"] = $matches[1];
		$kurz["nakup"] = (double) str_replace(",",".",$matches[2]);
		$kurz["predaj"] = (double) str_replace(",",".",$matches[3]);
		// dump($kurz);
		return $kurz["nakup"];
	}

	public static function rate_uah(){
		$content = HTTP::get("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode=EUR&date=20190327");
		if($content==''){ return false; };
		$xml = simplexml_load_string($content);
		utf8_decode($xml);
		return (float) $xml->currency->rate;
	}

	public static function rate($curr,$timestamp=0){
		if($curr==Currency::$native){
			return 1;
		}
		if($timestamp>0){ $Date=new Timestamp((int)$timestamp); } else { $Date=new Timestamp(); };

		$UseCache = class_exists('Cache');

		$hash = $curr.$Date->show('Ymd');
		/** @var \Kubomikita\Commerce\IDiConfigurator $context */
		$context = Registry::get("container");
		if($context->isProtein() && $curr == "CZK"){
			$currency_settings = (float) Params::val("currency_czk");
			if($currency_settings > 0){
				return $currency_settings;
			}
			return 27;
		}

		$cache = new Caching\Cache($context->getService("cache.storage.cz"),"currency");

		$currency = $cache->load($hash, function () use ($curr,$timestamp){
			return (Currency::rate_ecb_get($curr,$timestamp)+0.7);
		});

		/*if($currency < 24){
			$interval_sms = 60*60*5;
			$r = $context->getService("database")->query("SELECT datetime FROM ec_users_action WHERE objectId = 'CZK_warning' ORDER BY id DESC LIMIT 1")->fetch();
			$last_sms = $r->datetime->getTimestamp();
			$secs_sms = time()-$last_sms;
			//dump("Sek. od poslendej SMS: ".$secs_sms,"Sek. po kt. sa odosle dasia SMS: ".$interval_sms."; kurz:".$currency);
			if($secs_sms > $interval_sms){
				LogAction::add("Odoslaná SMS o zlyhani stiahntia kurzu CZK",null,null,"CZK_warning");

				Sms::send("+421911531872","Kurz CZK je mensi ako 20. Kurz: ".$currency);
				Sms::send("+421914777222","Kurz CZK je mensi ako 20. Kurz: ".$currency);
			}
			$cache->clean([Caching\Cache::NAMESPACES => ["currency"]]);
			$currency = 28;
		}*/

		$ret = $currency;


		/**if(!$context->isProduction() && $context->isDebugMode()){
			$cache = new Caching\Cache($context->getService("cache.storage"),"currency");
			$currency = $cache->load($hash,function(&$dependencies) use($curr,$timestamp){
				$dependencies = [
					Caching\Cache::EXPIRE => "12 hours"
				];
				return (Currency::rate_ecb_get($curr,$timestamp)+0.7);
			});

			return $currency;
		}

		if($UseCache && Cache::check('currency', $hash)){ $ret=Cache::get('currency', $hash); } else {
			$ret = (Currency::rate_ecb_get($curr,$timestamp)+0.7);
			//dump($ret);
			if($ret > 24) {
				if ( $UseCache ) {
					Cache::put( 'currency', $hash, $ret );
				};
			} else {
				$interval_sms = 60*60*5;
				$Q1 = mysqli_query(CommerceDB::$DB,"SELECT datetime FROM ec_users_action WHERE objectId='CZK_warning' ORDER BY id DESC LIMIT 1");
				$R1 = mysqli_fetch_assoc($Q1);

				$last_sms = strtotime($R1["datetime"]);

				$secs_sms = time()-$last_sms;

				dump("Sek. od poslendej SMS: ".$secs_sms,"Sek. po kt. sa odosle dasia SMS: ".$interval_sms."; kurz:".$ret);
				//LogAction::add("Odoslaná SMS o zlyhani synchronizácie Pohody.",null,null,"SMS_warning");
				if($secs_sms > $interval_sms){
					LogAction::add("Odoslaná SMS o zlyhani stiahntia kurzu CZK",null,null,"CZK_warning");
					dump("poslat spravu");
					//Sms::send("+421911531872","Kurz CZK je mensi ako 20. Kurz: ".$ret);
					//Sms::send("+421914777222","Kurz CZK je mensi ako 20. Kurz: ".$ret);
				}
				if ( $UseCache ) {
					Cache::del( "currency", $hash );
				}
				$ret = 28;
			}
		};
		 */

		return $ret;
	}

	public static function rate_papi($currency,$timestamp = 0){
		if($currency==Currency::$native){
			return 1;
		}
		if($timestamp>0){ $Date=new Timestamp((int)$timestamp); } else { $Date=new Timestamp(); };

		$UseCache = class_exists('Cache');

		$hash = $currency.$Date->show('Ymd');
		/** @var \Kubomikita\Commerce\IDiConfigurator $context */
		$context = Registry::get("container");

		$cache = new Caching\Cache($context->getService("cache.storage"),"currency");

		$currency = $cache->load($hash, function () use ($currency){
			if($currency == "UAH"){
				return Currency::rate_uah();
			}
			return ( Currency::rate_ecb_get( $currency ) );

		});
		if($currency > 0){
			return $currency;
		}

		$c = [
			"UAH" => 30.582,
			"USD" => 1.1261,
			"PLN" => 4.2936,
			"EUR" => 1
		];

		return $c[$currency];

	}






}
