<?php

class HTTP {

	public static function get($url, $timeout=1){
		if(function_exists("curl_init")){
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt ($ch, CURLOPT_URL, $url);
			curl_setopt ($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			ob_start();
			curl_exec ($ch);
			curl_close ($ch);
			$string = ob_get_contents();
			ob_end_clean();
			return $string;
		} else {
			return file_get_contents($url);
		}
	}


}

