<?php
use Deployment\Deployer;
use Deployment\Logger;
use Deployment\Server;
use Contributte\Deployer\Config\Config;
use Contributte\Deployer\Config\Section;
use Contributte\Deployer\Listeners\BeforeListener;
use Nette\Utils\ArrayHash;

class DeployListener implements BeforeListener
{

	protected $searchFor = [];
	protected $replaceFor = [];
	protected $files = [];

	private function loadConfig($webname) {
		$this->searchFor = [
			"ErrorDocument 404 /".$webname."/",
			"RewriteBase /".$webname."/",
			"RewriteRule ^imgcache/(.*)$ /".$webname."/imgcache/$1",
			"RewriteRule ^imgs/(.*)$ /".$webname."/imgs/$1",
			"RewriteRule ^imgi/(.*)$ /".$webname."/imgi/$1",
			"RewriteRule ^imgc/(.*)$ /".$webname."/imgc/$1",
			"RewriteRule ^img.php$ /".$webname."/img.php",
			"RewriteRule ^(.*)/$ /".$webname."/$1 [L,R=301]"
		];
		$this->replaceFor = [
			"ErrorDocument 404 /",
			"RewriteBase /",
			"RewriteRule ^imgcache/(.*)$ /imgcache/$1",
			"RewriteRule ^imgs/(.*)$ /imgs/$1",
			"RewriteRule ^imgi/(.*)$ /imgi/$1",
			"RewriteRule ^imgc/(.*)$ /imgc/$1",
			"RewriteRule ^img.php$ /img.php",
			"RewriteRule ^(.*)/$ /$1 [L,R=301]"
		];

		$this->files = [".htaccess","imgcache/.htaccess","commerce/manager/.htaccess"];
	}


	/**
	 * @param Config $config
	 * @param Section $section
	 * @param Server $server
	 * @param Logger $logger
	 * @param Deployer $deployer
	 *
	 * @throws \Nette\Application\AbortException
	 */
	public function onBefore(Config $config, Section $section, Server $server, Logger $logger, Deployer $deployer): void
	{
		//$logger->log("before");

	}
	/**
	 * @param Config $config
	 * @param Section $section
	 * @param Server $server
	 * @param Logger $logger
	 * @param Deployer $deployer
	 * @return void
	 * @throws Exception
	 */
	public function onAfter(Config $config, Section $section, Server $server, Logger $logger, Deployer $deployer) :void
	{
		$webname = $section->getName();
		$this->loadConfig($webname);
		foreach($this->files as $file){
			$p = file_get_contents($section->getLocal()."/".$file);
			$new = str_replace($this->searchFor,$this->replaceFor,$p);
			file_put_contents($section->getLocal()."/".$file.".production",$new);
			try {
				$server->writeFile( $section->getLocal() . "/" . $file . ".production",
					$server->getDir() . "/" . $file );
				$logger->log("File: ".$file." changed to production and uploaded.");
			} catch (\Deployment\ServerException $e) {
				$logger->log("File: ".$file." change ERROR - ".$e->getMessage(),"red");
			}
			//unlink($section->getLocal()."/".$file.".production");

		}

		$this->betaCheck($config,$section,$server,$logger);
		$this->imgcacheFlush($config,$logger);
		$this->appcacheFlush($config,$logger);
		$this->bash($server);
	}
	public function onAfter1(Config $config, Section $section, Server $server, Logger $logger, Deployer $deployer) :void
	{
		$logger->log("after");

	}

	protected function betaCheck(Config $config, Section $section, Server $server, Logger $logger){
		$ud = $config->getUserdata();
		if(isset($ud["beta"]) && $ud["beta"]){
			$dir = $section->getLocal(). "/commerce/";
			$original = str_replace("-beta","",$ud["beta_bootstrap"]);
			if(!isset($ud["beta_bootstrap"]) || !file_exists($dir.$ud["beta_bootstrap"])){
				throw new \Nette\Application\AbortException("bootstrap file must be filled or exists");
			}
			file_put_contents($dir.$original.".beta",file_get_contents($dir.$ud["beta_bootstrap"]));
			$server->writeFile($dir.$original.".beta",$server->getDir()."/commerce/".$original);
			$logger->log("Beta bootstrap uploaded!");
			//unlink($dir.$original.".beta");

		}
	}
	protected function imgcacheFlush(Config $config, Logger $logger){
		$ud = $config->getUserdata();
		$cond = isset($ud["imgcacheFlush"]) ? (bool) $ud["imgcacheFlush"] : true;
		if(isset($ud["weburl"]) && $cond){
			$url = $ud["weburl"]."imgcache/flush.php";
			$d = HTTP::get($url);
			$logger->log("ImgCache Flush ended - ".$url);

		}
	}
	protected function appcacheFlush(Config $config, Logger $logger){
		$ud = $config->getUserdata();
		$cond = isset($ud["cacheFlush"]) ? (bool) $ud["cacheFlush"] : true;
		if(isset($ud["weburl"]) && $cond){
			$url = $ud["weburl"]."api/cache/flush";
			$d = HTTP::get($url);
			$logger->log("AppCache Flush ended - ".$url);

		}
	}
	protected function bash(Server $server){
		//dump($server,$server->execute("php -v"));
	}
	public function afterDeploySSH(Config $config, Section $section, Server $server, Logger $logger, Deployer $deployer) : void {
		$userdata = ArrayHash::from( $config->getUserdata() );

		if ( ! empty( $userdata->ssh2->commands ) ) {
			$conn = ssh2_connect( $userdata->ssh2->host );
			ssh2_auth_password( $conn, $userdata->ssh2->user, $userdata->ssh2->pass );

			if ( ! $conn ) {
				$logger->log( "Failed to connect " . $userdata->ssh2->user . "@" . $userdata->ssh2->host, "red" );
			} else {

				foreach ( $userdata->ssh2->commands as $command ) {
					$stream = ssh2_exec( $conn, $command );
					stream_set_blocking( $stream, true );
					$stream_out = ssh2_fetch_stream( $stream, SSH2_STREAM_STDIO );
					$logger->log( "cmd: " . $command, "aqua" );
					$result = stream_get_contents( $stream_out );
					if ( strlen( $result ) > 0 ) {
						$logger->log( $result, "lime" );
					} else {
						$logger->log( "executed", "fuchsia" );
					}
				}

				ssh2_disconnect( $conn );
			}
		}
	}
}