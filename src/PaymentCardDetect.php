<?php

namespace Kubomikita\Utils;
use \CardDetect\Detector;
use Nette\Utils\Strings;

class PaymentCardDetect{
	/**
	 * @var string
	 */
	private $cardNumber;
	/**
	 * @var array
	 */
	private $cardTypes = [
		"fab fa-cc-visa" => 'Visa',
		"fab fa-cc-amex" => 'Amex',
		"fab fa-cc-mastercard" => 'MasterCard',
		"fab fa-cc-discover" => 'Discover',
		"fab fa-cc-jcb" => 'JCB',
		"fab fa-cc-diners-club" => 'DinersClub'
	];

	/**
	 * PaymentCardDetect constructor.
	 *
	 * @param string $cardNumber
	 */
	public function __construct(string $cardNumber) {
		$this->cardNumber = $this->sanitizeCardNumber($cardNumber);
	}

	/**
	 * @param string $cardNumber
	 */
	private function sanitizeCardNumber(string $cardNumber): string {
		if(Strings::contains($cardNumber,"*")){
			return str_replace("*","0",$cardNumber);
		}
		return $cardNumber;

	}
	public function detect( string $card = null ): string {
		$checkCard = $this->cardNumber;
		if($card !== null){
			$checkCard = $this->cardNumber = $card;
		}
		foreach ($this->cardTypes as $cardType) {
			$method = 'is' . $cardType;
			if ($this->$method($checkCard)) {
				return $cardType;
			}
		}
		return 'Invalid Card';
	}

	public function getFaIcon(){
		return array_search($this->detect(),$this->cardTypes);
	}

	private function isVisa(string $card) : bool
	{
		return preg_match("/^4[0-9]{0,15}$/i", $card);
	}
	private function isMasterCard(string $card) :  bool
	{
		return preg_match("/^5[1-5][0-9]{5,}|222[1-9][0-9]{3,}|22[3-9][0-9]{4,}|2[3-6][0-9]{5,}|27[01][0-9]{4,}|2720[0-9]{3,}$/i", $card);
	}
	private function isAmex(string $card) : bool
	{
		return preg_match("/^3$|^3[47][0-9]{0,13}$/i", $card);
	}
	private function isDiscover(string $card) : bool
	{
		return preg_match("/^6$|^6[05]$|^601[1]?$|^65[0-9][0-9]?$|^6(?:011|5[0-9]{2})[0-9]{0,12}$/i", $card);
	}
	private function isJCB(string $card) : bool
	{
		return preg_match("/^(?:2131|1800|35[0-9]{3})[0-9]{3,}$/i", $card);
	}
	private function isDinersClub(string $card) : bool
	{
		return preg_match("/^3(?:0[0-5]|[68][0-9])[0-9]{4,}$/i", $card);
	}
}

class PaymentCardDetectTest {
	private $cards;
	public function __construct() {
		$this->cards = [
			'Visa' => '4111111111111111',
			'MasterCard' => '5555555555554444',
			'MasterCard1' => '2221000000000009',
			'MasterCard2' => '2223000048400011',
			'MasterCard3' => '2223016768738313',
			'MasterCard4' => '5105510551055105',
			'Discover' => '6011111111111117',
			'Amex' => '378282246310005',
			'Amex1' => '371449635398431',
			'AmexCorp' => '378734493671000',
			'JCB' => '3530111333300000',
			'JCB1' => '3566002020360505',
			'Diners' => '30569309025904',
			'Diners1' => '38520000023237',
			'Invalid' => '7877787778777877'
		];
	}
	public function run(){
		foreach ($this->cards as $key => $cardNumber){
			$detector = new PaymentCardDetect($cardNumber);
			echo $key .' - '.$cardNumber." - ".$detector->detect();
			echo "\n<br>";
		}
	}
}